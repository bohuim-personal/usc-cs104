/**
 * bst.h
 *  Implements a(n unbalanced) BST storing Key,Value pairs
 */

#include <iostream>
#include <exception>
#include <cstdlib>
#include <utility>

/* -----------------------------------------------------
 * Regular Binary Tree Nodes
 ------------------------------------------------------*/

template <class KeyType, class ValueType>
class Node
{
    protected:
        std::pair<const KeyType, ValueType> _item;
        Node<KeyType, ValueType>* _left;
        Node<KeyType, ValueType>* _right;
        Node<KeyType, ValueType>* _parent;

    public:
        // the default is to create new nodes as leaves
        Node(const KeyType& k, const ValueType& v, Node<KeyType, ValueType> *p) : _item(k, v)
        { 
            _parent = p; 
            _left = _right = NULL;
        }
  
        virtual ~Node(){}

        std::pair<const KeyType, ValueType> const& getItem() const
        {
            return _item;
        }
      
        std::pair<const KeyType, ValueType>& getItem()
        {
            return _item;
        }
        
        const KeyType& getKey() const
        {
            return _item.first;
        }

        const ValueType& getValue() const
        { 
            return _item.second;
        }
      
        /* the next three functions are virtual because for Red-Black-Trees,
         we'll want to use Red-Black nodes, and for those, the 
         getParent, getLeft, and getRight functions should return 
         Red-Black nodes, not just ordinary nodes.
         That's an advantage of using getters/setters rather than a struct. */
      
        virtual Node<KeyType, ValueType>* getParent() const
        { 
            return _parent;
        }
      
        virtual Node<KeyType, ValueType>* getLeft() const
        {
            return _left;
        }
      
        virtual Node<KeyType, ValueType>* getRight() const
        {
            return _right;
        }
      
        void setParent(Node<KeyType, ValueType> *p)
        { 
            _parent = p;
        }
      
        void setLeft(Node<KeyType, ValueType> *l)
        { 
            _left = l;

            if (l)
                l->setParent(this);
        }
      
        void setRight(Node<KeyType, ValueType> *r)
        { 
            _right = r;

            if (r)
                r->setParent(this);
        }
      
        void setValue(const ValueType &v)
        { 
            _item.second = v;
        }


};

/* -----------------------------------------------------
 * Regular Binary Search Tree
 ------------------------------------------------------*/

template <class KeyType, class ValueType>
class BinarySearchTree
{
    protected:
        // Main data member of the class
        Node<KeyType, ValueType> *root;
        unsigned int _size;

    public:
        typedef std::pair<const KeyType, ValueType> value_type;
        typedef Node<KeyType, ValueType> BSTNode;

        /**
         * Constructor
         */
        BinarySearchTree()
        {
            root = NULL;
            _size = 0;
        }

        /**
         * Destructor
         */
        ~BinarySearchTree()
        { 
            deleteAll(root); 
        }

        unsigned int size()
        {
            return _size;
        }

        /**
         * @method insert
         * 
         * Inserts the given value_type pair into the tree
         */
        virtual void insert(const value_type& item)
        {
            root = insertHelper(root, item);
            _size++;
        }

        /**
         * Prints the entire tree structure in a nice format 
         *  
         * It will denote subtrees in [] brackets.
         *  This could be helpful if you want to debug your functions. 
         */  
        void print() const
        { 
            printRoot(root);
            std::cout << "\n";
        }
    
        /**
         * An In-Order iterator
         */
        class iterator
        {
            protected:
                Node<KeyType, ValueType>* curr;

            public:
                /**
                 * Initialize the internal members of the iterator
                 */
                iterator(Node<KeyType,ValueType>* ptr)
                {
                    curr = ptr;
                }
                
                std::pair<const KeyType,ValueType>& operator*()
                {
                    return curr->getItem();
                }
                
                std::pair<const KeyType,ValueType>* operator->() 
                { 
                    return &(curr->getItem());
                }
        
                /**
                 * Checks if 'this' iterator's internals have the same value
                 *  as 'rhs'
                 */
                bool operator==(const iterator& rhs) const
                {
                    if (!curr || !rhs.curr)
                        return (!curr && !rhs.curr);

                    return curr->getValue() == rhs.curr->getValue();
                }
        
                /**
                 * Checks if 'this' iterator's internals have a different value
                 *  as 'rhs'
                 */
                bool operator!=(const iterator& rhs) const
                {
                    return !(*this == rhs);
                }
        
                /**
                 * @operator ++it
                 * 
                 * Pre-increment of iterator.
                 * Moves this iterator to the next position
                 * and returns the same iterator after incrementing.
                 */
                iterator& operator++()
                {
                    if (curr == NULL)
                        return *this;

                    BSTNode *right = curr->getRight();
                    if (right)
                    {
                        BSTNode *smallest = curr->getRight();
                        while(smallest->getLeft())
                            smallest = smallest->getLeft();
                        curr = smallest;
                        return *this;
                    }
                    else
                    {
                        using namespace std;
                        BSTNode *target = curr;
                        BSTNode *parent = curr->getParent();
                        while(parent && target == parent->getRight())
                        {
                            target = parent;
                            parent = target->getParent();
                        }

                        curr = parent;
                        if (parent && target != parent->getLeft()) //if the last connection is not less than, curr was the last node
                            curr = NULL;

                        return *this;
                    }
                }

                /**
                 * @operator it++
                 * 
                 * Post-increment of iterator.
                 * Makes a copy of this, increments this
                 * and returns the dummy copy that hasn't been incremented.
                 */
                iterator operator++(int)
                {
                    iterator it(curr);
                    ++(*this);
                    return it;
                }
        };
  
        /**
         * Returns an iterator to the "smallest" item in the tree
         */
        iterator begin()
        {
            BSTNode *smallest = root;
            if (!smallest)
                return end();

            while(smallest->getLeft())
                smallest = smallest->getLeft();
            return iterator(smallest);
        }

        /**
         * Returns an iterator whose value means INVALID
         */
        iterator end()
        {
            return iterator(NULL);
        }

        /**
         * Returns an iterator to the item with the given key, k
         * or the end iterator if k does not exist in the tree
         */
        iterator find(const KeyType & k) const 
        {
            Node<KeyType, ValueType> *curr = internalFind(k);
            iterator it(curr);
            return it;
        }
  
    protected:
        /**
         * @method getRoot
         * 
         * Returns the root.
         */
        virtual BSTNode* getRoot() const
        {
            return root;
        }

        /**
         * @method setRoot
         * 
         * Sets the given node as root
         */
        virtual void setRoot(BSTNode *node)
        {
            root = node;
        }

        /**
         * @method insertHelper
         * 
         * Inserts the given item into tree with root.
         */
        BSTNode* insertHelper(BSTNode *root, const value_type& item)
        {
            if (root == NULL)
                return new BSTNode(item.first, item.second, root);

            if (item.first == root->getKey())       //match
                root->setValue(item.second);
            else if (item.first < root->getKey())   //less than
                root->setLeft( insertHelper(root->getLeft(), item) );
            else                                    //bigger than
                root->setRight( insertHelper(root->getRight(), item) );
            return root;
        }

        /**
         * Helper function to find a node with given key, k and 
         * return a pointer to it or NULL if no item with that key
         * exists
         */
        Node<KeyType, ValueType>* internalFind(const KeyType& k) const 
        {
            Node<KeyType, ValueType> *curr = root;
            while (curr) 
            {
                if (curr->getKey() == k)
                {
	               return curr;
                }
                else if (k < curr->getKey())
                {
	               curr = curr->getLeft();
                }
                else
                {
	               curr = curr->getRight();
                }
            }
            return NULL;
        }
  
        /**
         * Helper function to print the tree's contents
         */
        virtual void printRoot(Node<KeyType, ValueType> *r) const
        {
            if (r != NULL)
            {
	            std::cout << "[";
	            printRoot(r->getLeft());
                std::cout << " (" << r->getKey() << ", " << r->getValue() << ") ";
            	printRoot(r->getRight());
            	std::cout << "]";
            }
        }
  
        /**
         * Helper function to delete all the items
         */
        void deleteAll(Node<KeyType, ValueType> *r)
        {
            if (r != NULL)
            {
	           deleteAll (r->getLeft());
	           deleteAll (r->getRight());
	           delete r;
            }
        }
};
