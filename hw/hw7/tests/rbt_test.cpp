#include <iostream>
#include <string>
#include <utility>

#include "../rbbst.h"

using namespace std;

int main(int argc, char* argv[])
{
	RedBlackTree<int, int> rbt;

	//EMPTY
	cout << "Checking iterator begin == end for empty tree" << endl;
	if (rbt.begin() == rbt.end()){}
	else
	{
		cerr << "ERROR: iterator begin does not equal end for empty tree" << endl;
		return 1;
	}

	//INSERT TEST
	cout << "Testing each succeeding value is greater than or equal to previous" << endl;

	int insert;
	srand(time(NULL));
	for (int i=0; i<20; i++)
	{
		insert = rand() % 20;
		rbt.insert( make_pair(insert, 0) );

		cout << "Inserting: " << insert << endl;
		rbt.print();
		cout << endl;
	}

	int val;
	int last = -1;
	RedBlackTree<int, int>::iterator it = rbt.begin();
	while(it != rbt.end())
	{
		val = it->first;

		if (val < last)
		{
			cout << "ERROR: returned " << val << " is less than previous " << last <<endl;
			return 1;
		}

		last = val;
		++it;
	}

	cout << "All tests passed!" << endl;

  	return 0;
}