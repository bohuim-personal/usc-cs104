#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <utility>
#include <cstdlib>
#include <ctime>

#include "rbbst.h"

using namespace std;

/* Structs */
typedef struct Rect
{
	Rect(int id, int l, int h)
	{
		ID = id;
		length = l;
		height = h;
	}

	int ID;
	int length;
	int height;

} Rect;

typedef struct Point
{
	Point()
	{
		x = -1;
		y = -1;
	}

	int x;
	int y;

} Point;


/* Prototypes */
typedef RedBlackTree<int, Rect*> RBTRect;
typedef RedBlackTree<int, Rect*>::iterator RectIter;
typedef RedBlackTree<int, Point*> RBTPoint;
typedef RedBlackTree<int, Point*>::iterator PointIter;

void run(const char* ifile, const char* ofile);
bool solve(int **board, const int& nRows, const int& nCols, RectIter rectIt, PointIter pointIt, int tilesLeft);
void rotate(Rect *rect);
bool canBeTiled(Rect *rect, int **board, const int& x, const int& y);
void tile(Rect *rect, Point *point, int **board, const int& x, const int& y);
void untile(Rect *rect, Point *point, int **board, const int& x, const int& y);

void printRect(Rect *rect, Point *point, ostream& os);
void printBoard(int **board, const int& nRows, const int& nCols);


/* Main */
int main(int argc, char* argv[])
{
	if (argc < 3)
	{
		cerr << "Provide both an input and output filename" << endl << endl;
		return 1;
	}

	run(argv[1], argv[2]);
	return 0;
}


/* Functions */
void run(const char* ifile, const char* ofile)
{
	ifstream is(ifile);
	ofstream os(ofile);

	RBTRect rects;
	RBTPoint points;

	// Get board dim & # of tiles
	int nTiles;
	int nCols, nRows;
	is >> nCols >> nRows >> nTiles;

	int **board = new int*[nRows];
	for (int row = 0; row < nRows; ++row)
	{
		board[row] = new int[nCols];
		for (int col = 0; col < nCols; ++col)
			board[row][col] = -1;
	}

	// Create tiles
	int id, tLength, tHeight;
	for (int i=0; i < nTiles; ++i)
	{
		is >> id >> tLength >> tHeight;
		rects.insert( make_pair(id, new Rect(id, tLength, tHeight)) );
		points.insert( make_pair(id, new Point()) );
	}

	// Solve
	srand(time(0));
	if ( solve(board, nRows, nCols, rects.begin(), points.begin(), nTiles) )
	{
		printBoard(board, nRows, nCols);

		RectIter rectIt = rects.begin();
		PointIter pointIt = points.begin();
		while (rectIt != rects.end())
		{
			printRect(rectIt->second, pointIt->second, os);
			++rectIt;
			++pointIt;
		}
	}
	else
	{
		os << "No solution found" << endl; 
	}

	// Free all Rects and points
	RectIter rectIt = rects.begin();
	PointIter pointIt = points.begin();
	while (rectIt != rects.end())
	{
		delete rectIt->second;
		delete pointIt->second;

		++rectIt;
		++pointIt;
	}

	//clear board
	for (int row = 0; row < nRows; ++row)
		delete[] board[row];
	delete[] board;

	is.close();
	os.close();
}

/**
 * @solve
 * 
 * Solves 
 *
 * @param board		- 2D bool board for keeping track of taken tiles
 * @param nRows		- height of board
 * @param nCols		- length of board
 * @param it 	 	- RBTRectree Iterator containing current Rect
 * @param tilesLeft - number of tiles left
 */
bool solve(int **board, const int& nRows, const int& nCols, RectIter rectIt, PointIter pointIt, int tilesLeft)
{
	if (tilesLeft == 0)
		return true;

	//setup next iterator
	Rect *rect = rectIt->second;
	Point *point = pointIt->second;
	RectIter nextRect = rectIt;
	PointIter nextPoint = pointIt;
	++nextRect;
	++nextPoint;

	//before rotate
	for (int row = 0; row < (nRows - rect->height + 1); ++row)
		for (int col = 0; col < (nCols - rect->length + 1); ++col)
			if ( canBeTiled(rect, board, row, col) )
			{
				tile(rect, point, board, row, col);
				if ( solve(board, nRows, nCols, nextRect, nextPoint, tilesLeft-1) )
					return true;
				untile(rect, point, board, row, col);
			}

	//rotate
	rotate(rect);

	//after rotate
	for (int row = 0; row < (nRows - rect->height + 1); ++row)
		for (int col = 0; col < (nCols - rect->length + 1); ++col)
			if ( canBeTiled(rect, board, row, col) )
			{
				tile(rect, point, board, row, col);
				if ( solve(board, nRows, nCols, nextRect, nextPoint, tilesLeft-1) )
					return true;
				untile(rect, point, board, row, col);
			}

	//rotate back
	rotate(rect);

	return false;
}

/**
 * @method rotate
 * 
 * Rotates the given rect
 */
void rotate(Rect *rect)
{
	int temp = rect->length;
	rect->length = rect->height;
	rect->height = temp;
}

/**
 * @method canBeTile
 * 
 * Returns whether the given rect can be tiled 
 * onto the board at the given (x,y) coordinates.
 * 
 * @param rect 	- Rect to untile
 * @param board - 2D bool board to keep track of taken spots
 * @param x		- x coordinate 
 * @param y 	- y coordinate
 */
bool canBeTiled(Rect *rect, int **board, const int& row, const int& col)
{
	for (int r = row; r < (row + rect->height); ++r)
	{
		for (int c = col; c < (col + rect->length); ++c)
		{
			if(board[r][c] > -1)
				return false;
		}
	}
	return true;
}

/**
 * @method tile
 * 
 * Given a rect, board, and coordinates on the board,
 * tiles the rect on the board: makes it true.
 * @note assumes the x+height & y+length does not exceed the bounds of the board. 
 * 
 * @param rect 	- Rect to untile
 * @param board - 2D bool board to keep track of taken spots
 * @param x		- x coordinate 
 * @param y 	- y coordinate
 */
void tile(Rect *rect, Point *point, int **board, const int& row, const int& col)
{
	int color = rand() % 8;

	for (int r = row; r < (row + rect->height); ++r)
	{
		for (int c = col; c < (col + rect->length); ++c)
			board[r][c] = color;
	}
	point->x = col;
	point->y = row;
}

/**
 * @method untile
 * 
 * Given a rect, a board, and coordinates on the board,
 * untiles the rect from the board: makes it false.
 * @note assumes the x+height & y+length does not exceed the bounds of the board.
 *
 * @param rect 	- Rect to untile
 * @param board - 2D bool board to keep track of taken spots
 * @param x		- x coordinate 
 * @param y 	- y coordinate
 */
void untile(Rect *rect, Point *point, int **board, const int& row, const int& col)
{
	for (int r = row; r < (row + rect->height); ++r)
	{
		for (int c = col; c < (col + rect->length); ++c)
			board[r][c] = -1;
	}
	point->x = -1;
	point->y = -1;
}

/**
 * @method printRect
 * 
 * Prints the given rect to the ostream.
 */
void printRect(Rect *rect, Point *point, ostream& os)
{
	os << (rect->ID) << " ";
	os << (point->x)  << " " << (point->y) << " ";
	os << (rect->length) << " " << (rect->height) << endl;
}

/**
 * @method printBoard
 * 
 * Prints board to cout
 */
void printBoard(int **board, const int& nRows, const int& nCols)
{
	for (int row = nRows-1; row >= 0; --row)
	{
		cout << "|";
		for (int col = 0; col < nCols; ++col)
		{
			int val = board[row][col];
			if (val == -1)
				cout << " ";
			else
			{
				cout << "\033[;3" << val << "m";
				cout << "@";
				cout << "\033[0m";
			}
			cout << "|"; 
		}
		cout << endl;
	}
}

