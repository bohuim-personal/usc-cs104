Homework 7
==========

All work has been done.
To compile all files

```shell
$ make
```


### Problem 2 & 3
Answers to these are given in their respective `problem <#>.jpg`


### Problem 4 & 5
BST Iterator is tested with RedBlackTree.

To test the trees:
```shell
$ ./bin/rbt_test
```


### Problem 6
To test rectangles:
```shell
$ ./bin/rectangles <inputfile> <outputfile>
```

**Clean up**
```shell
$ make clean
```