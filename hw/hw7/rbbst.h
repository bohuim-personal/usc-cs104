/*
 * rbbst.h
 *
 * Date        Author    Notes
 * =====================================================
 * 2014-04-14  Kempe     Initial version
 * 2015-04-06  Redekopp  Updated formatting and removed
 *                         KeyExistsException
 */

#include <iostream>
#include <exception>
#include <cstdlib>
#include <cmath>
#include <iomanip>
#include "bst.h"

/* -----------------------------------------------------
 * Red-Black Nodes and Search Tree
 ------------------------------------------------------*/

enum Color {red, black};

template <class KeyType, class ValueType>
class RedBlackNode : public Node<KeyType, ValueType>
{
    public:
        RedBlackNode(KeyType k, ValueType v, RedBlackNode<KeyType, ValueType> *p) 
        : Node<KeyType, ValueType>(k, v, p)
        { 
            color = red;
        }
  
        virtual ~RedBlackNode () {}
  
        Color getColor () const
        {
            return color;
        }
  
        void setColor (Color c)
        {
            color = c;
        }
  
        virtual RedBlackNode<KeyType, ValueType>* getParent() const
        {
            return (RedBlackNode<KeyType,ValueType>*) this->_parent;
        }
  
        virtual RedBlackNode<KeyType, ValueType>* getLeft() const
        {
            return (RedBlackNode<KeyType,ValueType>*) this->_left;
        }
  
        virtual RedBlackNode<KeyType, ValueType>* getRight() const
        {
            return (RedBlackNode<KeyType,ValueType>*) this->_right;
        }
  
    protected:
        Color color;
};

/* -----------------------------------------------------
 * Red-Black Search Tree
 ------------------------------------------------------*/

template <class KeyType, class ValueType>
class RedBlackTree : public BinarySearchTree<KeyType, ValueType>
{
    public:
        typedef BinarySearchTree<KeyType, ValueType> BST;
        typedef typename BST::value_type value_type;
        typedef RedBlackNode<KeyType, ValueType> RBNode;

        /**
         * @method insert
         * 
         * Inserts the given value_type pair into this RBTree.
         * First just insert the new value like BSTree.
         * If the returned pointer is null (key exists, value updated)
         *      - do nothing and return.
         * Otherwise
         *      - if root is null, set new node as root.
         *      - fixTree at the newNode
         *      - color root black
         */
        virtual void insert(const value_type& new_item)
        {
            RBNode *newNode = regularInsert(getRoot(), new_item);
            if (newNode == NULL)
                return;

            //first insert
            if (getRoot() == NULL)
                setRoot(newNode);

            fixTree(newNode);
            getRoot()->setColor(black);
            BST::_size++;
        }

        /**
         * @method print
         * 
         * Prints this tree using printRoot()
         */
        void print()
        {
            printRoot(getRoot());
            std::cout << std::endl;
        }

    protected:
        /**
         * @method getRoot
         * 
         * Returns the root as a RBNode
         */
        virtual RBNode* getRoot() const
        {
            return (RBNode *)BST::root;
        }

        /**
         * @method setRoot
         * 
         * Sets the given node as the root.
         */
        virtual void setRoot(RBNode *node)
        {
            BST::root = node;
        }

        /**
         * @method regularInsert
         * 
         * Given a current, recursively inserts the given item.
         * Upon hitting an empty node, creates and returns a new node.
         * If key already exists, updates value and returns NULL.
         *
         * @param current - RBNode tree to insert into
         * @param item    - pair of key, value to insert
         * @return pointer to new RBNode, or NULL if key exists
         */
        RBNode* regularInsert(RBNode *current, const value_type& item)
        {
            if (current == NULL)
                return new RBNode(item.first, item.second, NULL);

            //less or greater
            RBNode *newNode = NULL;
            if (item.first == current->getKey())
            {
                current->setValue(item.second);
            }
            else if (item.first < current->getKey())
            {
                newNode = regularInsert(current->getLeft(), item);
                if (current->getLeft() == NULL)
                    current->setLeft(newNode);
            }
            else if (item.first > current->getKey())
            {
                newNode = regularInsert(current->getRight(), item);
                if (current->getRight() == NULL)
                    current->setRight(newNode);
            }
            return newNode;
        }

        /**
         * @method fixTree
         * 
         * Fixes the tree at the given node.
         * If node doesn't have a parent or parent is black: done.
         * Otherwise,
         *      if no uncle or uncle is black, rotate.
         *      if uncle is red, recolor and fixTree on grandparent.
         */
        void fixTree(RBNode *node)
        {
            //not root & not null
            RBNode *parent = node->getParent();
            if (parent == NULL || parent->getColor() == black)
                return;

            RBNode *uncle = getSibling(parent);
            if (uncle == NULL || uncle->getColor() == black)
            {
                RBNode *grandparent = parent->getParent();
                RBNode *newTop = NULL;

                if (node == parent->getLeft())
                {
                    if(parent == grandparent->getLeft())
                    {
                        rightRotate(parent);
                        newTop = parent;
                    }
                    else
                    {
                        rightRotate(node);
                        leftRotate(node);
                        newTop = node;
                    }
                }
                else //node is right of parent
                {
                    if(parent == grandparent->getLeft())
                    {
                        leftRotate(node);
                        rightRotate(node);
                        newTop = node;
                    }
                    else
                    {
                        leftRotate(parent);
                        newTop = parent;
                    }
                }

                colorBlack(newTop);
                if (grandparent == getRoot())
                {
                    setRoot(newTop);
                    newTop->setParent(NULL);
                }
            }
            else //uncle is red
            {
                uncle->setColor(black);
                parent->setColor(black);

                RBNode *grandparent = parent->getParent();
                if (grandparent)
                {
                    grandparent->setColor(red);
                    fixTree(grandparent);
                }
            }
        }

        /**
         * @method getSibling
         * 
         * Returns the sibling of the given node.
         * If no parent, then no sibling.
         * Otherwise, return child of parent thats not node.
         */
        RBNode* getSibling(RBNode *node)
        {
            RBNode *parent = node->getParent();
            if (parent == NULL)
                return NULL;

            if (node == parent->getLeft())
                return parent->getRight();
            return parent->getLeft();
        }

        /**
         * @method leftRotate
         * 
         * Rotates tree left using given node as pivot.
         * Node's parent is now the left child of node,
         * and parent's right becomes null.
         *
         * If grandparent existed,
         * node replaces parent as direct child.
         */
        void leftRotate(RBNode *node)
        {
            RBNode *parent = node->getParent();
            if (parent == NULL)
                return;

            RBNode *grandparent = parent->getParent();
            parent->setRight(node->getLeft());
            node->setLeft(parent);

            if (grandparent)
            {
                if (parent == grandparent->getLeft())
                    grandparent->setLeft(node);
                else
                    grandparent->setRight(node);
            }
        }

        /**
         * @method rightRotate
         * 
         * Rotates tree right using given node as pivot.
         * Node's parent is now the right child of node,
         * and parent's left becomes null.
         *
         * If grandparent existed,
         * node replaces parent as direct child.
         */
        void rightRotate(RBNode *node)
        {
            using namespace std;
            RBNode *parent = node->getParent();
            if (parent == NULL)
                return;

            RBNode *grandparent = parent->getParent();
            parent->setLeft(node->getRight());
            node->setRight(parent);

            if (grandparent)
            {
                grandparent->setColor(red);
                if (parent == grandparent->getLeft())
                    grandparent->setLeft(node);
                else
                    grandparent->setRight(node);
            }
        }

        void colorBlack(RBNode *node)
        {
            RBNode *left = node->getLeft();
            RBNode *right = node->getRight();

            node->setColor(black);
            if (left)  left->setColor(red);
            if (right) right->setColor(red);
        }

        virtual void printRoot(RBNode *r) const
        {
            using namespace std;

            if (r != NULL)
            {
                cout << "[";
                printRoot(r->getLeft());

                int color = 0;
                if (r->getColor() == red)
                    color = 1;
                cout << "\033[;3" << color << "m (" << r->getKey() << ", " << r->getValue() << ") \033[0m";

                printRoot(r->getRight());
                cout << "]";
            }
        }

};

