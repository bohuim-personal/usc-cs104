#include "util.h"

#include <iostream>
#include <sstream>
#include <cctype>
#include <algorithm>
using namespace std;

string convToLower(string src) 
{
	transform(src.begin(), src.end(), src.begin(), ::tolower);
	return src;
}

/**
 * @method parseStringToWords
 * 
 * Given a raw string, parses it.
 * Separate by space and remove punctuations.
 * After punctuations are stripped, 
 * if the word is longer than 2 letters,
 * the word is added to a set of words.
 * 
 * @param rawWords - string to parse
 */
set<string> parseStringToWords(string rawWord) 
{
	set<string> words;

	string word;
	istringstream wordStream(rawWord);
	while ( getline(wordStream, word, ' ') )
	{
		string temp = "";
		for (unsigned int i=0; i < word.length(); i++)
		{
			if(!ispunct(word[i]))
				temp += word[i];
			else
			{
				if(temp.length() >= 2)
					words.insert( convToLower(temp) );
				temp = "";
			}
		}
		if(temp.length() >= 2)
			words.insert( convToLower(temp) );
	}

	return words;
}
