#ifndef MOVIE_H
#define MOVIE_H

#include "product.h"
using namespace std;

class Movie : public Product
{
	public:
		Movie(string title, string genre, string rating, double price, int qty);
		~Movie();

		set<string> keywords() const;
		bool isMatch(vector<string>& searchTerms) const;
		string displayString() const;
		void dump(ostream& os) const;

	private:
		string genre;
		string rating;
};

#endif