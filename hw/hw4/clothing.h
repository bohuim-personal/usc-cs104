#ifndef CLOTHING_H
#define CLOTHING_H

#include "product.h"
using namespace std;

class Clothing : public Product
{
	public:
		Clothing(string title, string size, string brand, double price, int qty);
		~Clothing();

		set<string> keywords() const;
		bool isMatch(vector<string>& searchTerms) const;
		string displayString() const;
		void dump(ostream& os) const;

	private:
		string size;
		string brand;
};

#endif