Homework 4/Project 1
--------------------

Program desc:
- `Amazon`: main body of program. Takes command and arguments from user regarding searching, adding, viewing, and buying.
- `DataManager`: Does the work given by `Amazon.cpp` by keeping track of all data: products, users, and carts.
- `Product` and derived classes: Represents one type of item that can be shopped
- `Product_Parser`: Parses a database text file into objects.

All the work is done, simply run  
```shell
$ make
$ ./bin/amazon <database filename>
```

After testing, to quit application,
type in the program
```shell
QUIT <output db filename>
```
