#ifndef DATAMANGER_H
#define DATAMANGER_H

#include "datastore.h"
#include <ostream>
#include <utility>
#include <vector>
#include <map>
#include <set>

class DataManager : public DataStore
{
	public:
		DataManager();
		~DataManager();

		/* DataStore override */
		void addUser(User* u);
		void addProduct(Product* p);
		void dump(std::ostream& ofile);
		std::vector<Product*> search(std::vector<std::string>& terms, int type);

		/* Cart Methods */
		void buyCart(const std::string& username);
		void printCart(const std::string& username);
		void addToCart(const std::string& username, Product* product);

    private:
    	std::vector<User*> users;
    	std::vector<Product*> products;
    	std::map< std::string, User* > userMap;
    	std::map< std::string, std::set<Product*> > keyIndex;
    	std::map< std::string, std::vector<Product*> > carts;
};

#endif