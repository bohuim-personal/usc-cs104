#include "clothing.h"
#include "util.h"

#include <iostream>
#include <sstream>
using namespace std;

/**
 * @method Clothing
 * 
 * Constructor for clothing.
 * Takes in necessary params for both clothing and Product
 * Passes the Product vars to Product's constructor
 * and sets the others
 *
 * @param title  - of clothing
 * @param size 	 - of clothing
 * @param brand  - of clothing
 * @param price  - of product
 * @param qty    - initial quantity
 */
Clothing::Clothing(string name, string size, string brand, double price, int qty) : Product("clothing", name, price, qty)
{
	this->size = size;
	this->brand = brand;
}

Clothing::~Clothing()
{

}

/* Override */
/**
 * @method keywords
 * 
 * Returns the set of keywords associated with this clothing.
 * Runs clothing's name and title through parseStringToWords
 * and gets the union of the two returned sets.
 * Adds ISBN to the set (doesn't need to be parsed).
 *
 * @return set of keyword that relate to this clothing
 */
set<string> Clothing::keywords() const
{
	set<string> brandWords = parseStringToWords(brand);
	set<string> nameWords = parseStringToWords(name_);
	set<string> keywords = setUnion(brandWords, nameWords);
	keywords.insert(size);

	return keywords;
}

/**
 * @method isMatch
 * 
 * Given a list of search terms,
 * checks whether they match with this clothing
 * 
 * @param searchTerms - to check with this clothing
 * @return whether given search terms match this clothing
 */
bool Clothing::isMatch(vector<string>& searchTerms) const
{
	set<string> keys = keywords();
	for (unsigned int i=0; i<searchTerms.size(); i++)
		if ( keys.find(searchTerms[i]) != keys.end() )
			return true;
	return false;
}

/**
 * @method displayString
 * 
 * Returns the description of this clothing
 * in a specific format for displaying.
 * 
 * @param formatted summary string of this clothing
 */
string Clothing::displayString() const
{
	ostringstream converter;

	//main desc
	string display = "";
	display += name_ + "\n";
	display += "Size: " + size + " " + "Brand: " + brand + "\n";

	//price
	converter << price_;
	display += converter.str() + " ";

	//qty left
	converter.str("");
	converter.clear();
	converter << qty_;
	display += converter.str() + " left" + "\n";

	return display;
}

/**
 * @method dump
 * 
 * Given an output stream, 
 * writes the info for this clothing
 * in a specified database format.
 *
 * @param os - outputstream to write db info to
 */
void Clothing::dump(ostream& os) const
{
	os << "clothing" 	<< endl;
	os << getName() 	<< endl;
	os << getPrice()	<< endl;
	os << getQty()  	<< endl;
	os << size 			<< endl;
	os << brand 		<< endl;
}