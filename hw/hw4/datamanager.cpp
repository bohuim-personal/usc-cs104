#include "datamanager.h"

#include "util.h"
#include <iostream>
using namespace std;

/* Helper prototypes */
void removePunct(string& word);

/**
 * @method DataManager
 * 
 * Constructor for DataManager
 * Nothing to do
 */
DataManager::DataManager()
{

}

/**
 * @method ~DataManager
 * 
 * Destructor for DataManager
 * Delete all users and products
 */
DataManager::~DataManager()
{
	for (unsigned int i=0; i<products.size(); i++)
		delete products[i];

	for (unsigned int i=0; i<users.size(); i++)
		delete users[i];
}


/* DataStore Override */

/**
 * @method addUser
 * 
 * Given a pointer to a created User object,
 * adds it to the list(vector) of users,
 * and also maps the name to the pointer.
 * 
 * @param u - user pointer to add
 */
void DataManager::addUser(User* u)
{
	users.push_back(u);
	userMap[ convToLower(u->getName()) ] = u;
}

/**
 * @method addProduct
 * 
 * Given a pointer to a created product object,
 * adds it to the list(vector) of products.
 * Also gets the set of keywords associated,
 * and maps all keywords back to that product.
 * 
 * @param p - product pointer to add
 */
void DataManager::addProduct(Product* p)
{
	products.push_back(p);

	set<string> keywords = p->keywords();
	for (set<string>::iterator it = keywords.begin(); it != keywords.end(); it++)
		keyIndex[*it].insert(p);
}

/**
 * @method dump
 * 
 * Given an output stream to a database file,
 * writes all the updated products and users
 * to the database in the pre-defined format.
 * 
 * @param ofile - ref of output stream to file
 */
void DataManager::dump(ostream& ofile)
{
	ofile << "<products>" << endl;
	for (unsigned int i=0; i<products.size(); i++)
		products[i]->dump(ofile);
	ofile << "</products>" << endl;

	ofile << "<users>" << endl;
	for (unsigned int i=0; i<users.size(); i++)
		users[i]->dump(ofile);
	ofile << "</users>" << endl;
}

/**
 * @method search
 * 
 * Given search terms, and type of relation (AND/OR)
 * returns the vector of Products* that match the search.
 * 
 * @param terms - words to search with
 * @param type  - relation of terms (AND/OR)
 * @return vector of products meeting the search requirements
 */
vector<Product*> DataManager::search(vector<string>& terms, int type)
{
	vector<Product*> hits;
	if (terms.empty())
		return hits;

	set<Product*> match;
	for (unsigned int i=0; i<terms.size(); i++)
	{
		removePunct(terms[i]);

		//type 0: AND, type 1: OR
		if (type == 0) match = setIntersection(match, keyIndex[ terms[i] ]);
		else           match = setUnion(match, keyIndex[ terms[i] ]);
	}

	//http://stackoverflow.com/questions/5034211/c-copy-set-to-vector
	copy(match.begin(), match.end(), back_inserter(hits));
	return hits;
}


/* Cart Methods */
/**
 * @method buyCart
 * 
 * Given a username, fetches the corresponding
 * User and Cart(vector of Products).
 * For each product in the cart,
 * if quantity is available and user has enough $,
 * the item quantity & user balanced is subtracted.
 * 
 * @param username - of User to buy cart
 */
void DataManager::buyCart(const string& username)
{
	string userid = convToLower(username);
	if (userMap.find(userid) == userMap.end())
	{
		cout << "> User " << username << " not found" << endl;
		return;
	}

	User* user = userMap[userid];
	cout << "> " << userid << " purchased: ";

	int index = 0;
	vector<Product*> *cart = &carts[userid];
	while (index < (int)cart->size())
	{
		Product* product = cart->at(index);
		if ( (product->getQty() > 0) && (user->getBalance() >= product->getPrice()) )
		{
			product->subtractQty(1);
			user->deductAmount(product->getPrice());

			cart->erase(cart->begin() + index);
			index--;

			cout << product->getName() << ", ";
		}
		index++;
	}
	cout << endl;
	cout << "> Balance: $" << user->getBalance() << endl;
}

/**
 * @method printCart
 * 
 * Given a username, fetches the corresponding cart,
 * and prints the items by calling displayString for each.
 * 
 * @param username
 */
void DataManager::printCart(const string& username)
{
	string userid = convToLower(username);
	if (userMap.find(userid) == userMap.end())
	{
		cout << "> User " << username << " not found" << endl;
		return;
	}

	vector<Product*> *cart = &carts[userid];
	for (unsigned int i=0; i<cart->size(); i++)
	{
		cout << "Item " << i+1 << endl;
		cout << (*cart)[i]->displayString() << endl << endl;
	}
}

/**
 * @method addToCart
 * 
 * Given a username and a product,
 * retrieves the corresponding cart and adds the product
 * to the back of the cart (ordering does matter).
 * 
 * @param product  - Product* to add to cart
 * @param username - name of User adding product
 */
void DataManager::addToCart(const string& username, Product* product)
{
	string userid = convToLower(username);
	if (userMap.find(userid) == userMap.end())
	{
		cout << "> User " << username << " not found" << endl;
		return;
	}
	cout << product->getName() << endl;
	carts[userid].push_back(product);
	cout << "> Added to " << username << "'s cart: " << product->getName() << endl;
}


/* Helpers */
/**
 * @function removePunct
 * 
 * Given a ref to a string word,
 * removes all punctuations and replaces word.
 *
 * @param word - to remove punctuations from
 */
void removePunct(string& word)
{
	string newWord = "";
	for (unsigned int i=0; i<word.length(); i++)
		if (!ispunct(word[i]))
			newWord += word[i];
	word = newWord;
}








