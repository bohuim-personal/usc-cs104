#include "book.h"
#include "util.h"

#include <iostream>
#include <sstream>
using namespace std;

/**
 * @method Book
 * 
 * Constructor for book.
 * Takes in necessary params for both Book and Product
 * Passes the Product vars to Product's constructor
 * and sets the others
 *
 * @param title  - of book
 * @param author - of book
 * @param ISBN   - of book
 * @param price  - of product
 * @param qty    - initial quantity
 */
Book::Book(string title, string author, string ISBN, double price, int qty) : Product("book", title, price, qty)
{
	this->author = author;
	this->ISBN = ISBN;
}

/**
 * @method ~Book
 * 
 * Destructor for Book, nothing to delete
 */
Book::~Book()
{

}

/* Override */
/**
 * @method keywords
 * 
 * Returns the set of keywords associated with this book.
 * Runs book's name and title through parseStringToWords
 * and gets the union of the two returned sets.
 * Adds ISBN to the set (doesn't need to be parsed).
 *
 * @return set of keyword that relate to this book
 */
set<string> Book::keywords() const
{
	set<string> authorWords = parseStringToWords(author);
	set<string> nameWords = parseStringToWords(getName());
	set<string> keywords = setUnion(authorWords, nameWords);
	keywords.insert(ISBN);

	return keywords;
}

/**
 * @method isMatch
 * 
 * Given a list of search terms,
 * checks whether they match with this book
 * 
 * @param searchTerms - to check with this book
 * @return whether given search terms match this book
 */
bool Book::isMatch(vector<string>& searchTerms) const
{
	set<string> keys = keywords();
	for (unsigned int i=0; i<searchTerms.size(); i++)
		if ( keys.find(searchTerms[i]) != keys.end() )
			return true;
	return false;
}

/**
 * @method displayString
 * 
 * Returns the description of this book
 * in a specific format for displaying.
 * 
 * @param formatted summary string of this book
 */
string Book::displayString() const
{
	ostringstream converter;

	//main desc
	string display = "";
	display += getName() + "\n";
	display += "Author: " + author + " " + "ISBN: " + ISBN + "\n";

	//price
	converter << getPrice();
	display += converter.str() + " ";

	//qty left
	converter.str("");
	converter.clear();
	converter << getQty();
	display += converter.str() + " left" + "\n";

	return display;
}

/**
 * @method dump
 * 
 * Given an output stream, 
 * writes the info for this book
 * in a specified database format.
 *
 * @param os - outputstream to write db info to
 */
void Book::dump(ostream& os) const
{
	os << "book" 		<< endl;
	os << getName() 	<< endl;
	os << getPrice()	<< endl;
	os << getQty()  	<< endl;
	os << ISBN 			<< endl;
	os << author 		<< endl;
}