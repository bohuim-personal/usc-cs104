#ifndef UTIL_H
#define UTIL_H

#include <string>
#include <iostream>
#include <set>

using namespace std;

/* Custom Functions */


/* Given Functions */
string convToLower(string src);
set<string> parseStringToWords(string line);

template <typename T>
set<T> setIntersection(set<T>& s1, set<T>& s2)
{
	set<T> result;
	for(typename set<T>::iterator it = s1.begin(); it != s1.end(); ++it)
    {
	   if(s2.find(*it) != s2.end())
	       result.insert(*it);
	}
	return result;
}

template <typename T>
set<T> setUnion(set<T>& s1, set<T>& s2)
{
    set<T> result = s2;
    for(typename set<T>::iterator it = s1.begin(); it != s1.end(); ++it)
    {
        if(result.find(*it) == result.end())
            result.insert(*it);
    }
    return result;
}

#endif
