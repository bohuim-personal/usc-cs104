#ifndef PUZZLE_H
#define PUZZLE_H

#include <iostream>
#include <string>
#include <deque>
#include <map>

#include "board.h"
#include "puzzle_heur.h"
#include "puzzle_solver.h"

class Puzzle
{
	private:
		Board *board;
		PuzzleHeuristic *ph;

	public:
		Puzzle(int dim, int initMoves, int seed, int huerType);
		~Puzzle();

		void play();

	private:
		typedef std::map<int, Board*> map_type;
    	typedef map_type::iterator map_iter;

		std::deque<int> getSolution();

		void printBoard(Board* b) const;
		void printLabel(std::string& lb) const;
		void printCheat(int& cheatValue) const;
		void printMoves(map_type& moves) const;
		void cleanMoves(map_type& moves) const;
};

#endif