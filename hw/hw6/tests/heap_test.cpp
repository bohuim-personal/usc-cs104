#include "../heap.h"

#include <iostream>
#include <cstdlib>
#include <functional>
#include <vector>
#include <cmath>
#include <string>

using namespace std;

template <typename t, typename comp>
void printHeap(const Heap<t, comp>& heap, int m);

int main(int argc, char* argv[])
{
	int m = 2;
	Heap<int, less<int> > heap(m, less<int>());

	//CHECK EMPTY
	cout << "Checking heap is empty" << endl;
	if (!heap.empty())
	{
		cerr << "ERROR: heap not initialized empty" << endl;
		exit(1);
	}
	cout << endl;

	//CHECK ONE PUSH
	heap.push(20);
	cout << "Pushing 20"<< endl;
	cout << "CHECK: returned " << heap.top() << ", expected " << 20 << endl;
	printHeap(heap, m);
	if(heap.top() != 20)
	{
		cerr << "ERROR: top() returned " << heap.top() << ", expected " << 20 << endl;
		exit(1);
	}
	cout << endl;

	//CHECK ONE POP
	heap.pop();
	cout << "Popping top to check if empty" << endl;
	cout << "CHECK: empty [" << (heap.empty() ? "true" : "false") << "], expected [true]" << endl; 
	printHeap(heap, m);
	if(!heap.empty())
	{
		cerr << "ERROR: heap not empty after popping only value" << endl;
		exit(1);
	}
	cout << endl;

	//CHECK MULTIPLE PUSH
	cout << "Pushing 10 to 1" << endl;
	for (int i=10; i>=1; i--)
		heap.push(i);
	printHeap(heap, m);
	if (heap.top() != 1)
	{
		cerr << "ERROR: top() returned " << heap.top() << ", expected 1" << endl;
		exit(1);
	}
	cout << endl;

	//CHECK MULTIPLE POP
	cout << "Popping next three" << endl;
	for (int i=1; i<=3; i++)
	{
		int top = heap.top();
		heap.pop();

		cout << "CHECK: returned " << top << ", expected " << i << endl;
		if (top != i)
		{
			cerr << "ERROR: top() returned " << top << ", expected " << i << endl;
			exit(1);
		}
	}
	printHeap(heap, m);

	cout << "All tests passed for binary heap!" << endl;
}

template <typename t, typename comp>
void printHeap(const Heap<t, comp>& heap, int m)
{
	vector<t> list = heap.getList();
	int size = (int)list.size();
	int height = (size == 0) ? 0 : (log(size) / log(m))+1;
	int maxSize = pow(m, height)-1;

	int index = 0;
	for (int h = 0; h < height; h++)
	{
		int numBlocks = (maxSize / pow(m, h+1));

		for (int repeat = 0; repeat < pow(m, h); repeat++)
		{
			//first white space
			for (int b=0; b < numBlocks; b++)
				cout << "  ";

			//value
			if (index < size)
			{
				int val = list[index++];
				if (val < 10) cout << " ";
				cout << val;
			}
			else
				cout << "  ";

			//second whitespace
			for (int b=0; b < numBlocks; b++)
				cout << "  ";

			//fill
			cout << "  ";
		}
		cout << endl;

		int bLines = (height - h - m);
		for (int fill = 0; fill < pow(m, bLines)-1; fill++)
		{
			for (int repeat = 0; repeat < pow(m, h); repeat++)
			{
				for (int b = numBlocks-1; b >= 0; b--)
				{
					if(b == fill) cout << " /";
					else 		  cout << "  ";
				}
				cout << "  ";

				for (int b=0; b < numBlocks; b++)
				{
					if(b == fill) cout << " \\";
					else 		  cout << "  ";
				}
				cout << "  ";
			}
			cout << endl;
		}
	}
	cout << endl;
}

















