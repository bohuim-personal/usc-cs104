#include <iostream>
#include <cstdlib>

#include "puzzle.h"

using namespace std;

int main(int argc, char *argv[])
{
	if(argc < 5)
    {
		cerr << "Usage: ./puzzle size initMoves seed heur" << endl;
        return 1;
    }

  	int n = atoi(argv[1]);
  	int init = atoi(argv[2]);
  	int seed = atoi(argv[3]);
  	int heur = atoi(argv[4]);

  	if (n < 1 || 10 < n)
  	{
  		cerr << "Board's dimensions must be from 1 to 10" << endl;
  		return 1;
  	}
  	if (init < 0)
  	{
  		cerr << "Initial number of moves cannot be negative" << endl;
  		return 1;
  	}
  	if (heur < 0 || 2 < heur)
  	{
  		cerr << "Not a valid heuristic type" << endl;
  		return 1;
  	}

  	Puzzle game(n, init, seed, heur);
  	game.play();

	return 0;
}