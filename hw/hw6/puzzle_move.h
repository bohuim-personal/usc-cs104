#ifndef PUZZLEMOVE_H
#define PUZZLEMOVE_H

#include "board.h"
#include "puzzle_heur.h"

using namespace std;

struct PuzzleMove
{
    // Data members can be public
    PuzzleMove *prev;  // Pointer to parent PuzzleMove
    Board *board;  // Pointer to a board representing the updated state
    int tileMove;  // tile moved to reach the Board b
    int g;         // distance from the start board
    int h;         // heuristic distance to the goal
    int f;         // g + h

    /**
     * @method PuzzleMove
     * 
     * Constructor for root PuzzleMove
     * Takes only current(starting) board, and heuristic calculator.
     * Sets g as 0 and calculates (h, f) accordingly.
     * Parent is null.
     */
    PuzzleMove(Board *b, PuzzleHeuristic *ph)
    {
        prev = NULL;
        board = b;

        tileMove = 0;
        g = 0;
        h = ph->compute(*board);
        f = g + h;
    }

    /**
     * @method PuzzleMove
     * 
     * Constructor for PuzzleMove that has a previous move.
     * Takes in previous board, current board, 
     * tile used to get here, and heuristic calculator.
     * Sets appropriate (g, h, f) values.
     */
    PuzzleMove(int tile, Board* b, PuzzleMove *parent, PuzzleHeuristic *ph)
    {
        prev = parent;
        board = b;

        tileMove = tile;
        g = parent->g + 1;
        h = ph->compute(*board);
        f = g + h;
    }

    /**
     * @method ~PuzzleMove 
     * 
     * Destructor for PuzzleMove
     */
    ~PuzzleMove()
    {
        delete board;
    }
};

struct PuzzleMoveScoreComp
{
    bool operator()(const PuzzleMove *m1, const PuzzleMove *m2) const
    {
        if (m1->f < m2->f) return true;
        if (m1->f > m2->f) return false;

        //same
        return m1->h < m2->h;
    }
};

struct PuzzleMoveBoardComp
{
    bool operator()(const PuzzleMove *m1, const PuzzleMove *m2) const
    {  
        return *(m1->board) < *(m2->board);
    }
};

#endif
