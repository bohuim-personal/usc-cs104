#ifndef HEAP_CPP
#define HEAP_CPP

#include "heap.h"

#include <iostream>

using namespace std;

/**
 * @method Heap
 * 
 * Constructor for m-ary Heap
 * 
 */
template <typename T, typename Comparator>
Heap<T, Comparator>::Heap(int m, Comparator c) : n(m), comp(c)
{
	
}

/**
 * @method ~Heap
 * 
 * Destructor for m-ary heap
 */
template <typename T, typename Comparator>
Heap<T, Comparator>::~Heap()
{
    
}

template <typename T, typename Comparator>
vector<T> Heap<T, Comparator>::getList() const
{
    return list;
}


/* ------ Public Methods ---------------- */
/**
 * @method getList()
 *
 * Returns a copy of the underlying vector
 */
template <typename T, typename Comparator>
vector<T> Heap<T, Comparator>::getList() const
{
    return heap;
}

/**
 * @method empty
 * 
 * Returns whether heap is empty
 */
template <typename T, typename Comparator>
bool Heap<T, Comparator>::empty() const
{
	return list.empty();
}

/**
 * @method top
 * 
 * Returns the top element in the heap
 */
template <typename T, typename Comparator>
T const& Heap<T,Comparator>::top() const
{
  	if(empty())
    	throw logic_error("can't top an empty heap");

    return list.front();
}

/**
 * @method pop
 * 
 * Removes the top element from the heap
 */
template <typename T, typename Comparator>
void Heap<T, Comparator>::pop()
{
	if(empty())
    	throw logic_error("can't pop an empty heap");

    T last = list.back();
    list.pop_back();
    list[0] = last;
    heapifyDown(0);
}

/**
 * @method push
 * 
 * Adds the given element into the heap
 */
template <typename T, typename Comparator>
void Heap<T, Comparator>::push(const T& item)
{
	list.push_back(item);
	heapifyUp(list.size() - 1);
}


/* ------ Private Helpers ----------------*/
/**
 * @method heapifyUp
 * 
 * Given a location, takes the element and
 * shifts it up the tree until in the correct place.
 */
template <typename T, typename Comparator>
void Heap<T, Comparator>::heapifyUp(int index)
{
	int parentIndex = (index - 1) / n;
	T target = list[ index ];
	T parent = list[ parentIndex ];

	if ( comp(target, parent) )
	{
		list[ index ] = parent;
		list[ parentIndex ] = target;
		heapifyUp(parentIndex);
	}
}

/**
 * @method heapifyDown
 * 
 * Given a location, takes the element and
 * shifts it down the tree until in place.
 */
template <typename T, typename Comparator>
void Heap<T, Comparator>::heapifyDown(int index)
{
    int childIndex  = indexOfSmallestChild(index);
    if (childIndex < 0)
        return;

    T target = list[ index ];
    T child  = list[ childIndex ];
    if ( comp(child, target) )
    {
    	list[ index ] = child;
    	list[ childIndex ] = target;
		heapifyDown(childIndex);
    }
}

/**
 * @method indexOfSmallerChild
 * 
 * Returns the index of the smaller child
 * using the given index as the parent
 */
template <typename T, typename Comparator>
int Heap<T, Comparator>::indexOfSmallestChild(int index)
{
    int lIndex = (index * n) + 1;
    int rIndex = (index * n) + n;
	if (lIndex >= (int)list.size())
    	return -1;

    int childIndex = lIndex;
    T child = list[ childIndex ];
    for (int i = lIndex + 1; i < rIndex+1; i++)
    {
        if ( i < (int)list.size() && comp(list[i], child) )
        {
            child = list[i];
            childIndex = i;
        }
    }   
    return childIndex;
}

#endif



