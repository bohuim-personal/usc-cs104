#include "puzzle_solver.h"

#include <iostream>

using namespace std;

/**
 * @method PuzzleSolver
 * 
 * Constructor for PuzzleSolver
 * Given a board and heuristic type,
 * solves the board using A* algorithm.
 */
PuzzleSolver::PuzzleSolver(const Board &b, PuzzleHeuristic* ph)
: _b(b), _ph(ph), _expansions(0)
{

}

/**
 * @method ~PuzzleSolver
 * 
 * Destructor for PuzzleSolver
 * Nothing to be cleaned.
 */
PuzzleSolver::~PuzzleSolver()
{
	//no need to delete ph, maintained by Puzzle
	//all other variables are local
}

/**
 * @method run
 * 
 * Main method of PuzzleSolver class.
 * Run this method to obtain the solution and expansions,
 * which can be retrieved using the getter methods.
 */
void PuzzleSolver::run()
{
	//15-ary tree... because trial and error
	Heap<PuzzleMove*, PuzzleMoveScoreComp> open_list(15, PuzzleMoveScoreComp());
	PuzzleMoveSet closed_list;
	vector<PuzzleMove*> allMoves;

	//initial state
	PuzzleMove *initMove = new PuzzleMove(new Board(_b), _ph);
	allMoves.push_back(initMove);
	open_list.push(initMove);

	//while soln not found & open_list not empty
	while(!open_list.top()->board->solved())
	{
		//pop best guess, mark it, and increment expansion
		PuzzleMove *move = open_list.top();
		closed_list.insert(move);
		open_list.pop();

		//not solved: add all potential moves not already checked to open_list
		map_type nextMoves = move->board->potentialMoves();
		for (map_iter it = nextMoves.begin(); it != nextMoves.end(); ++it)
		{
			PuzzleMove *newMove = new PuzzleMove(it->first, it->second, move, _ph);
			if (closed_list.find(newMove) == closed_list.end())
			{
				_expansions++;
				open_list.push(newMove);
			}
			allMoves.push_back(newMove);
		}
	}

	//solved: stop search & backtrack parents for solution
	PuzzleMove *parent = open_list.top();
	do
	{
		int moveTile = parent->tileMove;
		if (moveTile != 0)
			_solution.push_front(parent->tileMove);
	}
	while( (parent = parent->prev) );

	//delete all puzzle moves used
	for (int i=0; i < (int)allMoves.size(); i++)
		delete allMoves[i];
}

/**
 * @method getSolution
 * 
 * Returns the solution found by run() in deque
 * @note solver.run() must have been run beforehand.
 */
deque<int> PuzzleSolver::getSolution()
{
	return _solution;
}

/**
 * @method getNumExpansions
 * 
 * Returns number of expansions(boards) checked 
 * @note solver.run() must have been run beforehand.
 */
int PuzzleSolver::getNumExpansions()
{
	return _expansions;
}


