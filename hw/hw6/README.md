#Homework 5/Project 3

### 2. Heaps
The answers are provided in jpg formats:
- `Problem 2(a).jpg`
- `Problem 2(b).jpg`


### 3. Heap Test
```shell
$ make heap_test
$ ./bin/heap_test
```


### 4. Puzzle
```shell
$ make puzzle
$ ./bin/puzzle <board dimension> <# initial moves> <seed> <heur type>
```

_Requirements_:
- `board dimension`: [1, 10]
- `# initial moves`: [0, int_max)
- `seed`: (int_min, int_max)
- `heur`: [0, 2]


### 5. Runtime
The runtime (or expansions) are included in `perf.txt`


**Clean up**
```shell
$ make clean
```