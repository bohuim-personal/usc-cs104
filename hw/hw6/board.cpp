#include "board.h"

#include <iostream>
#include <iomanip>
#include <map>
#include <cmath>
#include <cstdlib>
#include <stdexcept>

using namespace std;

/* ------- Con/Destructor -------------*/
/**
 * @method Board
 * 
 * Constructor for Board.
 * Init a board of given dimension (size = dim^2) and 
 * scramble it with numInitMoves by moving 
 * the space tile with a randomly chosen direction N, W, S, E
 * some of which may be invalid, in which case we skip that move
 */
Board::Board(int dim, int numInitMoves, int seed)
: _size(dim*dim), _dim(dim)
{
    _tiles = new int[_size];

    srand(seed);
    for(int i=0; i < _size; i++)
        _tiles[i] = i;
  
    int blankLoc = 0;
    while(numInitMoves > 0)
    {
        int r = rand()%4;
        int randNeighbor = -1;
        if(r == 0)
        {
            int n = blankLoc - dim;
            if(n >= 0)
	           randNeighbor = n;
        }
        else if(r == 1)
        {
            int w = blankLoc - 1;
            if(blankLoc % dim != 0)
	           randNeighbor = w;
        }
        else if(r == 2)
        {
            int s = blankLoc + dim;
            if(s  < _size)
	           randNeighbor = s;
        }
        else 
        {
            int e = blankLoc + 1;
            if(blankLoc % dim != dim-1)
	           randNeighbor = e;
        }

        if(randNeighbor > -1)
        {
            _tiles[blankLoc] = _tiles[randNeighbor];
            _tiles[randNeighbor] = 0;
            blankLoc = randNeighbor;
            numInitMoves--;
        }
    }
}

/**
 * @method Board
 * 
 * Copy constructor for board.
 * Takes in another board, and replicates its state.
 * @note will not work if passed in itself.
 *
 * @param rhs - other Board to copy
 */
Board::Board(const Board& rhs)
{
    if (this == &rhs)
        return;

    _dim = rhs._dim;
    _size = rhs._size;

    _tiles = new int[_size];
    for (int i = 0; i < _size; ++i)
        _tiles[i] = rhs._tiles[i];
}

/**
 * @method ~Board
 * 
 * Destructor for Board.
 * Free the memory used by tiles.
 */
Board::~Board()
{
    delete[] _tiles;
}


/* ------- Accessors -------------- */
/**
 * @operator []
 * 
 * Returns a const ref to value at loc.
 * @note Value cannot be changed using
 *       `board[0] = 0;`
 */
const int& Board::operator[](int loc) const 
{ 
    return _tiles[loc]; 
}

/**
 * @method size
 * 
 * Returns size (linear) of board
 */
int Board::size() const 
{ 
    return _size; 
}

/**
 * @method dim
 * 
 * Returns the dim of board
 */
int Board::dim() const
{
    return _dim;
    //return static_cast<int>(sqrt(_size));
}


/* ------- Public Methods -------------*/
/**
 * @method solved
 * 
 * Returns whether the board is solved
 */
bool Board::solved() const
{
    for (int i=0; i < _size; ++i)
    {
        if (i != _tiles[i])
            return false;
    }
    return true;
}

/**
 * @method move
 * 
 * move a tile with the given value into the blank spot. 
 * @note If the specified tile is not adjacent to the blank, 
 *       we just return and leave the board unaffected.
 */
void Board::move(int tile)
{
    int side_dim = dim();
    int tr, tc, br, bc;

    // find tile row and column
    int i=-1;
    while(_tiles[++i] != tile);

    tr = i / side_dim; 
    tc = i % side_dim;

    // find blank row and column
    int j=-1;
    while(_tiles[++j] != 0);

    br = j / side_dim;
    bc = j % side_dim;

    if( abs(static_cast<double>(tr-br)) + abs(static_cast<double>(tc-bc)) != 1)
    {
        cout << "Invalid move of tile " << tile << " at ";
        cout << tr << "," << tc << " and blank spot at ";
        cout << br << "," << bc << endl;
        return;
    }
  
    // Swap tile and blank spot
    _tiles[j] = tile;
    _tiles[i] = 0;
}

/**
 * @method potentialMoves
 * 
 * Generate new boards representing all the potential moves of tiles into
 * the current blank tile location. The returned map should have
 * the key as the tile moved and the value as a new Board object with the
 * configuration reflecting the move of that tile into the blank spot
 */
map<int, Board*> Board::potentialMoves() const
{
    int blankLoc = 0;
    while(_tiles[ blankLoc ] != 0)
        ++blankLoc;

    int row = blankLoc / _dim;
    int col = blankLoc % _dim;

    map<int, Board*> nextMoves;
    for (int i=0; i<4; ++i)
    {
        int valueRow = row;
        int valueCol = col;

        if (i == 0) valueCol += 1; //right
        if (i == 1) valueCol -= 1; //left
        if (i == 2) valueRow += 1; //bottom
        if (i == 3) valueRow -= 1; //top

        if ( isValid(valueRow, valueCol) )
        {
            int valueLoc = (valueRow * _dim) + valueCol;
            int value = _tiles[ valueLoc ];

            Board *nextBoard = new Board(*this);
            nextBoard->_tiles[valueLoc] = 0;
            nextBoard->_tiles[blankLoc] = value;
            nextMoves[value] = nextBoard;
        }
    }

    return nextMoves;
}


/* ------- Operators -------------- */
/**
 * @operator <<
 * 
 * Prints the board in the desired 2D format
 */
ostream& operator<<(ostream& os, const Board& b)
{
    b.printRowBanner(os);
    for (int row = 0; row < b._dim; row++)
    {
        os << "|";
        for (int col = 0; col < b._dim; col++)
        {
            int loc = (row * b._dim) + col;
            int val = b[loc];

            if (val < 10) os << " ";
            if (val == 0) os << " |";
            else          os << val << "|";
        }
        os << endl;
        b.printRowBanner(os);
    }
    return os;
}

/**
 * @operator <
 * 
 * Checks if this board is less-than another.  We define less than
 * as a "string-style" comparison of the tile array (i.e. Starting by comparing
 * the 0-th tile in this Board and the other.
 * If this board's tile is less than the other board's tile value, return true
 * If they are equal, continue with the next tile location and repeat
 *
 * HUGE PERFORMANCE UPDATE:
 * Instead returning true and stopping only when tile1 < tile2.
 * Check if tile2 < tile1, and if so, return false.
 * This shortcuits a lot of the work that would otherwise have been done twice.
 */
bool Board::operator<(const Board& rhs) const
{
    for (int i = 0; i < _size-1; ++i)
    {
        if (_tiles[i] < rhs._tiles[i]) return true;
        if (_tiles[i] > rhs._tiles[i]) return false;
    }
    return false;
}


/* ------- Private Helpers ------------------*/
/**
 * @method isValid
 * 
 * Returns whether given (row, col) is valid
 */
bool Board::isValid(int row, int col) const
{
    return (0 <= row && row < _dim) && (0 <= col && col < _dim);
}

/**
 * @method printRowBanner
 * 
 * Prints the row banner at the top, bottom, and in between tile rows
 */
void Board::printRowBanner(ostream& os) const
{
    if (_dim == 0)
        return;

    os << '+';
    for (int i=0; i < _dim; ++i)
        os << "--+";
    os << endl;

    // int side_dim = dim();
    // if(side_dim == 0) 
    //     return;

    // os << '+';
    // for(int i=0; i < side_dim; i++)
    //     os << "--+";
    // os << endl;
}


