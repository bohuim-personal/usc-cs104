#ifndef PUZZLESOLVER_H
#define PUZZLESOLVER_H

#include <vector>
#include <deque>
#include <set>

#include "heap.h"
#include "board.h"
#include "puzzle_move.h"
#include "puzzle_heur.h"


class PuzzleSolver
{
    public:
        typedef std::set<PuzzleMove*, PuzzleMoveBoardComp> PuzzleMoveSet;
        typedef std::map<int, Board*> map_type;
        typedef map_type::iterator map_iter;

        PuzzleSolver(const Board &b, PuzzleHeuristic* ph);
        ~PuzzleSolver();

        void run();
        int getNumExpansions();
        std::deque<int> getSolution();

    private:
        Board _b;
        PuzzleHeuristic *_ph;
        int _expansions;
        std::deque<int> _solution;
};

#endif
