#include "puzzle.h"

#include <iostream>
#include <string>
#include <cstdlib>

using namespace std;

#define CODE_QUIT 	0
#define CODE_CHEAT -1

/**
 * @method Puzzle
 * 
 * Constructor for Puzzle
 */
Puzzle::Puzzle(int dim, int initMoves, int seed, int heurType)
{
	board = new Board(dim, initMoves, seed);

	if (heurType == 0) ph = new PuzzleBFSHeuristic();
	if (heurType == 1) ph = new PuzzleOutOfPlaceHeuristic();
	if (heurType == 2) ph = new PuzzleManhattanHeuristic();
}

/**
 * @method ~Puzzle
 * 
 * Destructor for Puzzle
 */
Puzzle::~Puzzle()
{
	delete board;
	delete ph;
}

/**
 * @method play
 * 
 * Main method of the game.
 * Prints board and asks user for input.
 * Takes actions depending on input.
 */
void Puzzle::play()
{
	int input = 0;
	string trash;
	bool cont = true;

	string label = "";
	int cheat = 0;
	deque<int> solution;

	while(cont && !board->solved())
	{
		map<int, Board*> moves = board->potentialMoves();

		printBoard(board);
		printLabel(label);
		printMoves(moves);
		printCheat(cheat);

		cout << "Input: ";
		cin >> input;

		if (cin.fail())
		{
			label = "That's not even a number";
			cin.clear();
			cin >> trash;
		}
		else if (input == CODE_QUIT)
		{
			cont = false;
		}
		else if (input == CODE_CHEAT)
		{
			solution = getSolution();
			if (!solution.empty())
				cheat = solution.front();
		}
		else if (moves.find(input) == moves.end())
		{
			label = "Not a valid input";
		}
		else
		{
			if (!solution.empty() && input == solution.front())
			{
				solution.pop_front();
				cheat = solution.front();
			}
			else
				solution.clear();

			delete board;
			board = new Board(*moves[input]);
		}
		cleanMoves(moves);
	}

	if (board->solved())
	{
		printBoard(board);
		cout << "Solved!" << endl;
	}
}



/* ------- Private Helpers -------------- */
/**
 * @method printCheat
 * 
 * Prints the cheat sequence to the solution
 * starting from the given board.
 */
deque<int> Puzzle::getSolution()
{
	PuzzleSolver solver(*board, ph);
	solver.run();

	deque<int> solution = solver.getSolution();
	int expansions = solver.getNumExpansions();

	cout << endl;
	cout << "Try this sequence: " << endl;
	for (deque<int>::iterator it = solution.begin(); it != solution.end(); ++it)
		cout << (*it) << " ";
	cout << endl;

	cout << "(Expansions = " << expansions << ")" << endl;

	return solution;
}

/**
 * @method printBoard
 * 
 * Prints board to cout after 2 line breaks.
 */
void Puzzle::printBoard(Board *b) const
{
	cout << endl << endl;
	cout << *b;
}

/**
 * @method printLabel
 * 
 * Prints message to cout and resets it.
 */
void Puzzle::printLabel(string& label) const
{
	cout << "> " << label << endl;
	label = "";
}

/**
 * @method printCheat
 * 
 * Prints next hint and reset it.
 */
void Puzzle::printCheat(int& cheat) const
{
	cout << "Cheat: ";
	if (cheat != 0)
		cout << cheat;
	cout << endl;	

	cheat = 0;
}

/**
 * @method printMoves
 * 
 * Prints valid moves to cout
 */
void Puzzle::printMoves(map_type& moves) const
{
	cout << "Moves: ";
	for (map_iter it = moves.begin(); it != moves.end(); ++it)
		cout << it->first << " ";
	cout << endl;
}

/**
 * @method cleanMoves
 * 
 * Deletes the Boards in the given moves map.
 */
void Puzzle::cleanMoves(map_type& moves) const
{
	for (map_iter it = moves.begin(); it != moves.end(); ++it)
		delete it->second;
}