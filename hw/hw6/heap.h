#ifndef HEAP_H
#define HEAP_H

#include <iostream>
#include <vector>
#include <stdexcept>

template <typename T, typename Comparator>
class Heap
{
    public:
        Heap(int m, Comparator c);
        ~Heap();

        //accessors
        std::vector<T> getList() const;

        //public
        bool empty() const;
        void push(const T& item);
        T const & top() const;
        void pop();

    private:
        int n;
        Comparator comp;
        std::vector<T> list;

        void heapifyUp(int index);
        void heapifyDown(int index);
        int  indexOfSmallestChild(int index);
};

#include "heap.cpp"

#endif

