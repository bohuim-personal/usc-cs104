#ifndef BOARD_H
#define BOARD_H

#include <iostream>
#include <map>

class Board
{
    public:
        Board(int dim, int numInitMoves, int seed);
        Board(const Board& rhs);
        ~Board();

        //Accessors 
        int const & operator[](int loc) const;
        int dim() const;
        int size() const;

        bool solved() const;
        void move(int tile);
        std::map<int, Board*> potentialMoves() const; 

        //Operators
        friend std::ostream& operator<<(std::ostream &os, const Board &b);
        bool operator<(const Board& rhs) const;
  
    private:
        bool isValid(int row, int col) const;
        void printRowBanner(std::ostream& os) const;

        int *_tiles; // Will point to an array
        int _size;   // Size of the array
        int _dim;
};


#endif
