#include "puzzle_heur.h"

#include <cstdlib>
#include <iostream>

using namespace std;

/**
 * @method compute
 * 
 * Computes heuristic value(h) for board using Manhattan Distance.
 * The total distance of every off tile from its correct location.
 */
int PuzzleManhattanHeuristic::compute(const Board& b)
{
	int dim = b.dim();
	int size = b.size();

	int dist = 0;
	for (int loc = 0; loc < size; loc++)
	{
		int val = b[loc];
		if (val != loc && val != 0)
		{
			int deltaRow = abs( (val / dim) - (loc / dim) );
			int deltaCol = abs( (val % dim) - (loc % dim) );
			dist += deltaRow + deltaCol;
		}
	}
	return dist;
}

/**
 * @method compute
 * 
 * Computes heuristic value(h) for board using OutOfPlace.
 * The total number of tiles not in the correct place.
 */
int PuzzleOutOfPlaceHeuristic::compute(const Board& b)
{
	int count = 0;
	for (int i=0; i < b.size(); i++)
	{
		if (i != b[i] && b[i] != 0)
			count++;
	}
	return count;
}

/**
 * @method compute
 * 
 * Computes heuristic value(h) for board using BFS.
 * No heuristic calculation.
 */
int PuzzleBFSHeuristic::compute(const Board& b)
{
	return 0;
}