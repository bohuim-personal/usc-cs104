Homework 1
----------

### Problem 1-2 
- The answers to these problems are provided in "hw1.txt"

### Problem 4
The given "perm.cpp" program has been fixed

To test:
- Using terminal from inside the hw1/ directory, compile using `g++ perm.cpp -o <executable filepath>`
- Execute the compiled program using `./<executable filepath> <argument>`
- Expected behavior: all permutations of the given argument are printed

### Problem 5
Given an input text file with two lines of integers,
they are made into two linked lists delimited by the new line.
Three operations (concatenate, removeEvents, findAverage) are 
implemented recursively and written to an output file after each operation.

To test:
- Prepare an input file with lines of integers to be interpreted as linked lists,
  and an output file to write the results.
- Compile program using `g++ problem5.cpp -o <executable filepath>`
- Execute the compiled program with `./<executable filepath> <input filepath> <output filepath>`
- Expected behavior: output file should now have the results written. 

NOTE: `< filepath>` is simply the filename if in the same directory.  
NOTE: Enclose `< filepath>` in quotes if it contains spaces
