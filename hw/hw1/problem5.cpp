#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>

using namespace std;

//Def node of linked list
struct Item {
	Item(int v, Item* n) { val = v; next = n; }
	int val;
	Item* next;
};

//main func declaration
Item*  concatenate(Item* head1, Item* head2);
void   removeEvens(Item* &head);
double findAverage(Item* head);
double averageHelp(Item* head, int total, int count);

//helpers
void printList(ostream& ofile, Item* head);
void readLists(char* inFile, Item* &head1, Item* &head2);
Item* makeList(string line);
void clean(Item* head);

/**
 * @function main
 * 
 * Make 3 head pointer (set to null)
 * The first two are passed to readLists()
 * to make two lists using values from a formatted file.
 *
 * The third is the combined list of the two above.
 * (Actually the same as head1 but for clarity purposes).
 *
 * A output stream to the given output file name is opened,
 * the three main functions are called, 
 * each one followed printList() that writes to the file.
 * 
 * Output stream is closed, and list(s) is cleaned.
 *
 * @param argc - number of arguments, including this filename
 * @param argv - the arguments with [0] being this filename
 */
int main(int argc, char* argv[])
{
	//check for args
	if (argc < 3)
	{
		cerr << "Provide both an input and output file" << endl;
		return 1;
	}
    
	Item* head1 = NULL;
	Item* head2 = NULL;
	Item* head3 = NULL;
	readLists(argv[1], head1, head2);

	ofstream out;
	out.open(argv[2]);

	//combine
	head3 = concatenate(head1, head2);
	printList(out, head3);

	//remove
	removeEvens(head3);
	printList(out, head3);

	//average
	double avg = findAverage(head3);
	printList(out, head3);
	out << avg << endl;

	out.close();

	clean(head3);
	head1 = head2 = head3 = NULL;
}


/* MAIN FUNCTIONS */
/**
 * @function concatenate
 *
 * Given two head pointer to two linked lists,
 * returns head pointer to combined list,
 * where the second is appended to the first.
 *
 * Base case: head1 is null (first list is empty)
 * 		just return second list
 * Otherwise, traverse to the end of first list until base case
 * and for any items beforehand, just set next as its next.
 *
 * @param head1 - head pointer to first linked list
 * @param head2 - head pointer to second linked list
 * @param Item* - head pointer to the "new" combined list
 */
Item* concatenate(Item* head1, Item* head2)
{
	if (head1 == NULL)
		return head2;
	head1->next = concatenate(head1->next, head2);
	return head1;
}

/**
 * @function removeEvens
 * 
 * Given an head pointer (by reference),
 * removes all even values in the list.
 *
 * Base case: no list (empty)
 * 		cannot remove from empty list, so do nothing and return.
 * Otherwise:
 * 		if odd, move onto next item
 * 		if even, set head to next & delete current. move onto next. 
 *
 * @param head - reference to head pointer of list to remove evens
 */
void removeEvens(Item* &head)
{
	if (head == NULL)
		return;

	if (head->val % 2 == 0)
	{
		Item* curr = head;
		head = curr->next;
		delete curr;
		removeEvens(head);
	}
	else
		removeEvens(head->next);
}

/**
 * @function findAverage
 * 
 * Given the head pointer to a list,
 * returns the average of the values.
 * Wrapper for averageHelp().
 *
 * @param head - pointer to first item of list to get average
 * @return double value of the average
 */
double findAverage(Item* head)
{
	return averageHelp(head, 0, 0);
}

/**
 * @function averageHelp
 * 
 * Given an Item pointer, total, and number of values,
 * recursive calculates average of values in the list.
 *
 * Base case: no list, average of 0 items is 0.
 * Base case: last item, calculate & return the average.
 * Otherwise, pass on total sum & count up until now.
 */
double averageHelp(Item* head, int total, int count)
{
	if (head == NULL)
		return 0;

	total += head->val;
	count++;

	if (head->next == NULL)
		return total/count;
	return averageHelp(head->next, total, count);
}

/* OTHER HELPERS */
/**
 * @function printList
 * 
 * Given an out stream and head pointer to a list,
 * write the values of the list on a line in the file.
 *
 * @param head - pointer to first item of list
 * @param ofile - output stream to file to write to
 */
void printList(ostream& ofile, Item* head)
{
	if(head == NULL)
    	ofile << endl;
	else
  	{
    	ofile << head->val << " ";
    	printList(ofile, head->next);    
  	}
}

/**
 * @function readList
 * 
 * Open a FILE stream to the given filename in read mode.
 * Each line represents a list with values separated by a spaces.
 * 
 * @param inFile - name of input file
 * @param head1 - ref. head pointer to first list to make
 * @param head2 - ref. head pointer to second list to make
 */
void readLists(char* inFile, Item* &head1, Item* &head2)
{
    ifstream input(inFile);
    
    for (int i=0; i<2; i++)
    {
        string line;
        getline(input, line);
        
        if (i == 0) head1 = makeList(line); 
        else        head2 = makeList(line);
    } 
    input.close();
}

/**
 * @makeList
 * 
 * Given a line of integers delimited by spaces,
 * makes a linked list with the integer values.
 * 
 * Base case: no string, return null.
 * Otherwise, 
 * 		find the index of the first space
 * 		set current Item's value as [0, index) converted to int
 * 		set next pointer to makeList & pass [index+1, length-1)
 *
 * @param  line - string line to convert into linked list
 * @return list with int values in line as values
 */
Item* makeList(string line)
{
    int length = line.length();
    if (length == 0)
        return NULL;
    
    int index = line.find(' ');
    int value = atoi(line.substr(0, index).c_str());
    return new Item(value, makeList(line.substr(index+1, length-1)));
}

/**
 * @function clean
 * 
 * Given a head pointer to a list,
 * cleans the list by deleting it Items.
 *
 * @param head - pointer to first item of list
 */
void clean(Item* head)
{
	if (head == NULL)
		return;

	Item* next = head->next;
	delete head;
	clean(next);
}
