Homework 3
----------

First, compile and create all object and test files using  
`make` in the hw3/ directory

### Problem 2
LListInt is provided in `llistint.h` and `llistint.cpp`

Assuming `make` has been executed, to use provided testers:
- `./bin/llistint_test`
- test should output no messages if LListInt was implemented correctly

To use custom tester:
- `g++ -g -Wall <testfilename>.cpp llistint.o -o <executable test>`
- Note: `<executable test>` should not be `bin/llistint_test`


### Problem 3
SetInt is provided in `setint.h` and `setint.cpp`

Assuming `make` has been executed, to use provided testers:
- `./bin/setint_test`
- test should output no messages if SetInt was implemented correctly

To use custom tester:
- `g++ -g -Wall <testfilename>.cpp setint.o -o <executable test>`
- Note: `<executable test>` should not be `bin/setint_test`


### Problem 4
Assuming `make` has been executed, to parse a "book":
- `./bin/pgindex <input file> <output file> <var args>`
- `<input file>` is the relative path to the input text file 
- `<output file>` is the relative path to the output text file
- `<var args>` are the words to index separated by space characters


Run `make clean` to remove `bin/` folder