#include "llistint.h"
#include <cstdlib>

LListInt::LListInt()
{
    head_ = NULL;
    tail_ = NULL;
    size_ = 0;
}

LListInt::~LListInt()
{
    clear();
}

bool LListInt::empty() const
{
    return size_ == 0;
}

int LListInt::size() const
{
    return size_;
}

/**
 * @method copy constructor
 * 
 * Given another instance of LListInt,
 * copies over the values to this list.
 *
 * @param other - list to copy from
 */
LListInt::LListInt(const LListInt& other)
{
    head_ = NULL;
    tail_ = NULL;
    size_ = 0;

    for (Item* node = other.head_; node != NULL; node = node->next)
        this->push_back(node->val);
}

/**
 * @operator =
 * 
 * Defines the = operator for LListInt.
 * Only if this is not the same instance as the passed one,
 * copies over all items from other to this.
 *
 * @param other - list to copy from
 */
LListInt& LListInt::operator=(const LListInt& other)
{
    if (this != &other)
    {
        this->clear();
        for (Item* node = other.head_; node != NULL; node = node->next)
            this->push_back(node->val);
    } 
    return *this;
}

/**
 * @method push_back
 * 
 * Inserts the given val at the given value.
 * 
 * @param val - to insert
 */
void LListInt::push_back(const int& val)
{
    insert(size(), val);
}

/**
 * @method insert
 *
 * Inserts the value at the given location.
 * Method accounts for all edge cases.
 * @note nothing is modified if loc is not within [0, size]
 *
 * @param loc - to add new value at
 * @param val - new value to insert
 */
void LListInt::insert(int loc, const int& val)
{
    //check that loc is within bounds
    if (!(0 <= loc && loc <= size_))
        return;

    //assume loc == size
    Item* newNode = new Item;
    Item* oldNode = NULL;
    Item* preNode = tail_;
    if (loc < size_)
    {
        //otherwise get old node at loc & prev node
        oldNode = getNodeAt(loc);
        preNode = oldNode->prev;
    }

    //modify surrounding nodes accor.
    if (loc == 0) head_ = newNode;
    else  preNode->next = newNode;

    if (loc == size_) tail_ = newNode;
    else      oldNode->prev = newNode;

    //setup newly created node
    newNode->val = val;
    newNode->prev = preNode;
    newNode->next = oldNode;

    size_++;
}

/**
 * @method remove
 *
 * Removes the node at the given location.
 * Method accounts for all edge cases
 * @note nothing is modified if loc is not within [0, size).
 */
void LListInt::remove(int loc)
{
    //check loc is within bounds
    if (!(0 <= loc && loc < size_))
        return;

    //get target and surrounding nodes
    Item* node = getNodeAt(loc);
    Item* prev = node->prev;
    Item* next = node->next;

    //if node is first
    if (prev == NULL) head_ = next;
    else         prev->next = next;

    //if node is last
    if (next == NULL) tail_ = prev;
    else         next->prev = prev;

    //cleanup & decrement
    delete node;
    size_--;
}   

void LListInt::set(int loc, const int& val)
{
    Item *temp = getNodeAt(loc);
    temp->val = val;
}

int& LListInt::get(int loc)
{
    Item *temp = getNodeAt(loc);
    return temp->val;
}

int const & LListInt::get(int loc) const
{
    Item *temp = getNodeAt(loc);
    return temp->val;
}

void LListInt::clear()
{
    while(head_ != NULL)
    {
        Item *temp = head_->next;
        delete head_;
        head_ = temp;
    }
    tail_ = NULL;
    size_ = 0;
}


LListInt::Item* LListInt::getNodeAt(int loc) const
{
    Item *temp = head_;
    if(loc >= 0 && loc < size_)
    {
        while(temp != NULL && loc > 0)
        {
            temp = temp->next;
            loc--;
        }
        return temp;
    }
    else 
    {
        //throw std::invalid_argument("bad location");
        return NULL;
    }
}
