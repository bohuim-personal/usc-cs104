#include "../llistint.h"

#include <iostream>
#include <cstdlib>
using namespace std;

void checkListEqual(const LListInt& first, const LListInt& second)
{
	//check size
	if (first.size() != second.size())
	{
		cerr << "Size not equal" << endl;
		exit (1);
	}

	//check elements
	for (int i=0; i<first.size(); i++)
	{
		int val1 = first.get(i);
		int val2 = second.get(i);
		if (val1 != val2)
		{
			cerr << "Elements at index " << i << " not equal" << endl;
			cerr << "First[" << i << "] = " << val1 << ", Second[" << i << "] = " << val2 << endl;
			exit (1);
		}
	}
}

int main(int argc, char* argv[])
{
	//control list
	LListInt control;
	for (int i=0; i<10; i++)
		control.push_back(i);

	//copy constructor
	LListInt test1(control);
	checkListEqual(control, test1);

	//= to empty
	LListInt test2;
	test2 = control;
	checkListEqual(control, test2);

	//= to not empty
	LListInt test3;
	int val = 5;
	test3.push_back(val);
	test3 = control;
	checkListEqual(control, test3);

	//= to self
	control = control;
	checkListEqual(control, control);
}