#include "../setint.h"

#include <iostream>
#include <cstdlib>
using namespace std;

/**
 * @function printSet
 * 
 * Given a set, prints it.
 */
void printSet(SetInt& set)
{
	cout << "set: ";

	const int* int_p = set.first();
	while(int_p != NULL)
	{
		cout << *int_p << " ";
		int_p = set.next();
	}
	cout << endl;
}

/**
 * @function checkSetEquals
 * 
 * Given two sets, asserts that they are equal.
 * First checks size, and check every element from one is in the other.
 */
void checkSetEquals(SetInt& first, SetInt& second)
{
	//check size
	if (first.size() != second.size())
	{
		cerr << "Size not equal" << endl;
		exit (1);
	}

	//check elements
	const int* int_p = first.first();
	while(int_p != NULL)
	{
		if (!second.exists( *int_p ))
		{
			cerr << "Value from first " << *int_p << " not in second set" << endl;
			exit (1);
		}
		int_p = first.next();
	}

	int_p = second.first();
	while(int_p != NULL)
	{
		if (!first.exists( *int_p ))
		{
			cerr << "Value from second " << *int_p << " not in first set" << endl;
			exit (1);
		}
		int_p = second.next();
	}
}

/**
 * @checkSize
 * 
 * Given a set and expected size,
 * asserts that set's size is the expected size.
 * Otherwise prints the error and exits program.
 */
void checkSize(SetInt& set, int expected)
{
	if (set.size() != expected)
	{
		cerr << "Size not correct. Expected: " << expected << ", actual: " << set.size() << endl;
		exit (1);
	}
}

/**
 * @function checkValue
 * 
 * Given a set, pos, and expected value,
 * asserts that the value at that position is the expected.
 * Otherwise prints the error and exits program.
 */
void checkValue(SetInt& set, int expected)
{
	if (!set.exists(expected))
	{
		cerr << "Expected value " << expected << " does not exist";
		exit (1);
	}
}

void checkNotValue(SetInt& set, int unexpected)
{
	if (set.exists(unexpected))
	{
		cerr << "Unexpected value " << unexpected << " exists";
		exit (1);
	}
}

/**
 * @function main
 * 
 * Tests the SetInt class
 */
int main(int argc, char* argv[])
{
	SetInt test;
	printSet(test);
	checkSize(test, 0);

	//insert into empty
	test.insert(0);
	printSet(test);
	checkSize(test, 1);
	checkValue(test, 0);

	//insert into non-empty
	test.insert(1);
	printSet(test);
	checkSize(test, 2);
	checkValue(test, 1);

	//check removing doesnt remove anything else
	test.remove(3);
	printSet(test);
	checkSize(test, 2);
	checkValue(test, 0);
	checkValue(test, 1);

	//remove existing value
	test.remove(1);
	printSet(test);
	checkSize(test, 1);
	checkValue(test, 0);
	checkNotValue(test, 1);


	//controls
	SetInt control1;
	control1.insert(0);
	control1.insert(1);
	control1.insert(2);

	SetInt control2;
	control2.insert(2);
	control2.insert(3);
	control2.insert(4);

	SetInt control3; //empty

	//union
	SetInt unionSet = control1 | control2;
	printSet(unionSet);
	for (int i=0; i<5; i++)
		checkValue(unionSet, i);

	SetInt emptyUnion = control1 | control3;
	printSet(emptyUnion);
	for (int i=0; i<3; i++)
		checkValue(emptyUnion, i);

	//intersect
	SetInt intersectSet = control1 & control2;
	printSet(intersectSet);
	checkValue(intersectSet, 2);

	SetInt emptyInter = control1 & control3;
	printSet(emptyInter);
	checkSize(emptyInter, 0);
}








