#include "setint.h"
#include <cstdlib>

/**
 * @constructor SetInt
 *
 * Nothing to set
 */
SetInt::SetInt()
{

}

/**
 * @destructor ~SetInt
 * 
 * Nothing to dealloc
 */
SetInt::~SetInt()
{
	
}

/**
 * @method size
 * 
 * Return the size of the set,
 * which is the same as the list's size
 */
int SetInt::size() const
{
	return list_.size();
}

/**
 * @method empty
 * 
 * Returns whether set is empty,
 * by checking that size is 0.
 */
bool SetInt::empty() const
{
	return list_.size() == 0;
}

/**
 * @method insert
 * 
 * Inserts the given value to the set
 * if not already in the set.
 */
void SetInt::insert(const int& val)
{
	if (!exists(val))
		list_.push_back(val);
}

/**
 * @method remove
 * 
 * Removes the given value from the set,
 * by traversing and removing the value if found.
 */
void SetInt::remove(const int& val)
{
	for (int i=0; i<size(); i++)
		if (list_.get(i) == val)
		{
			list_.remove(i);
			return;
		}
	return;
}

/**
 * @method exists
 * 
 * Returns whether the given val is in the set
 * by traversing and returning true if found.
 * Otherwise false.
 */
bool SetInt::exists(const int& val) const
{
	for (int i=0; i<list_.size(); i++)
	{
		if (list_.get(i) == val)
			return true;
	}
	return false;
}

/**
 * @method first
 * 
 * Returns const pointer to first val of set,
 * or NULL if the set is empty.
 * @note first value is not guaranteed to be lowest value
 */
int const* SetInt::first()
{
	if (empty()) 
		return NULL;

	itPos = 0;
	return &list_.get(itPos);
}

/**
 * @method next
 * 
 * Returns const pointer to next val of set,
 * or NULL if position is at size.
 * @note given value is not in any particular order
 */
int const* SetInt::next()
{
	itPos++;
	if (itPos == size())
		return NULL;
	return &list_.get(itPos);
}

/**
 * @method setUnion
 * 
 * Given another set, returns a new set that is
 * union between this set and the second one.
 * Everything from this and other is inserted into
 * the newSet, which ensures that there will be no duplicates.
 *
 * @param other - const ref to second set to get union from
 * @return new set containing all elements from both sets
 */
SetInt SetInt::setUnion(const SetInt& other) const
{
	SetInt newSet;

	for (int i=0; i<this->size(); i++)
		newSet.insert(list_.get(i));

	//inserting will take care of duplicates
	for (int i=0; i<other.size(); i++)
		newSet.insert(other.list_.get(i));

	return newSet;
}

/**
 * @method setIntersection
 * 
 * Given another set, returns a new set that is
 * the intersection between this and the second one.
 * Only the elements that are in both sets are inserted.
 *
 * @param other - const ref to second set to get intersect from
 * @return new set containing all overlap elements from both sets
 */
SetInt SetInt::setIntersection(const SetInt& other) const
{
	SetInt newSet;

	for (int i=0; i<list_.size(); i++)
	{
		int val = list_.get(i);
		if (other.exists(val))
			newSet.insert(val);
	}

	return newSet;
}

/**
 * @operator |
 * 
 * Returns the union of this and other
 */
SetInt SetInt::operator |(const SetInt& other) const
{
	return this->setUnion(other);
}

/**
 * @operator &
 * 
 * Returns the intersections of this and other
 */
SetInt SetInt::operator &(const SetInt& other) const
{
	return this->setIntersection(other);
}

