#include "setint.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <algorithm>
#include <stdlib.h>
#include <cstdlib>
#include <map>
using namespace std;


bool getKey(string& word);
void stripPunct(string& word);
bool hasPunct(const string& word);
void parseBook(map<string, SetInt>& mymap, ifstream& in);
void parseLine(map<string, SetInt>& mymap, string& line, int& pageNumber);

void writePageForWord(map<string, SetInt>& mymap, ofstream& out, const char* word);
void printSet(SetInt& set, ofstream& out);

/**
 * @function main
 * 
 * main takes in 3+ arguments:
 * 	- argv[0] name of this file,
 * 	- argv[1] name input file
 * 	- argv[2] name output file
 *  - argv[3..?] words to print pages of
 *
 * Given these arguments,
 * i/o streams are created, along with a map of <string, SetInt>
 * and passed into parseBook, which inserts every word as a key.
 *
 * Then every word afterwards is passed into writePageForWord
 * with the map and output streams so that the pages that word
 * appears on is written to the file pointed by the output stream
 */
int main(int argc, char* argv[])
{
	if (argc < 3)
	{
		cerr << "Provide both an input/output file and words to search for" << endl;
		return 1;
	}

	ifstream in(argv[1]);
	ofstream out(argv[2]);

	map<string, SetInt> mymap;
	parseBook(mymap, in);

	int numWords = argc - 3;
	for (int i=0; i<numWords; i++)
		writePageForWord(mymap, out, argv[i + 3]);
}

/**
 * @function getKey
 * 
 * Given a string& word, converts it into a key.
 * First, stripPunct removes all punct from front & end.
 * Then if word still has punctuations in the middle
 * OR is not longer than 2 characters, returned as not key. 
 * If valid key, it's transformed to lowercase.
 *
 * Words as keys:
 * 	- must be 2 characters or characters
 *  - no punctuations in middle
 *  - all lower case
 * 
 * @param word - to convert into key
 * @return false if less than 2 characters; otherwise, yes.
 */
bool getKey(string& word)
{
	stripPunct(word);
	if (hasPunct(word) || word.length() < 2)
		return false;

	transform(word.begin(), word.end(), word.begin(), ::tolower);
	return true;
}

/**
 * @function stripPunct
 * 
 * Given a ref to a word, strips the punctuation 
 * from ONLY front or end using a recursive func.
 * Using a depth-first search,
 * recursively checks front and removes if punct
 * then moves onto end and removes if punct char.
 *
 * @param word - to remove punctuations from front & back
 */
void stripPunct(string& word)
{
	//check front
	if (ispunct(word[0]))
	{
		word = word.substr(1, word.length());
		stripPunct(word);
	}

	//check back
	if (ispunct(word[word.length()-1]))
	{
		word = word.substr(0, word.length()-1);
		stripPunct(word);
	}
}

/**
 * @function hasPunct
 * 
 * Returns whether given word has a punctuation
 * @param word - to check if has punctuation
 */
bool hasPunct(const string& word)
{
	int length = word.length();
	for (int i=0; i<length; i++)
	{
		if (ispunct(word[i]))
			return true;
	}
	return false;
}

/**
 * @function parseBook
 * 
 * Given a map and input stream,
 * parses the "book" into the map.
 * Starting from page number 1,
 * each line is parsed with parseLine()
 *
 * @param mymap - map to insert set into
 * @return in - input stream to read from
 */
void parseBook(map<string, SetInt>& mymap, ifstream& in)
{
	int page = 1;
	string line;
	while( getline(in, line) )
		parseLine(mymap, line, page);
}

/**
 * @function parseLine
 * 
 * Given a line from the "book"
 * parses the line by first getting tokens,
 * which are strings from line delimited by spaces.
 * Each token is then converted into a key,
 * which are strings from token delimited by hyphens.
 * Keys are filtered through getKey() func
 * and corresponding page is inserted into set stored by map.
 *
 * @param mymap - map to insert set into
 * @param line  - line from book to parse
 * @param page  - current page
 */
void parseLine(map<string, SetInt>& mymap, string& line, int& page)
{
	string token;
	istringstream tokenStream(line);
	while( getline(tokenStream, token, ' ') )
	{
		if(token == "<pagebreak>")
			page++;
		else
		{
			//separate hyphens 
			string key;
			istringstream keyStream(token);
			while( getline(keyStream, key, '-') )
			{
				//convert to key and insert page number
				if (getKey(key))
					mymap[key].insert(page);
			}
		}
	}
}

/**
 * @function writePageForWord
 * 
 * Given a map, output stream, and c string word,
 * converts the word into a c++ string key,
 * and prints the set at the key to the output stream.
 *
 * @param mymap
 * @param out   - out 
 * @param word  - c string from argv to check for
 */
void writePageForWord(map<string, SetInt>& mymap, ofstream& out, const char* word)
{
	string key(word);
	getKey(key);
	printSet(mymap[key], out);
}

/**
 * @function printSet
 * 
 * Given a set and an output stream,
 * prints the set to the stream on a new line.
 * None is written if set is empty.
 *
 * @param set - SetInt set to print from
 * @param out - output stream to write to
 */
void printSet(SetInt& set, ofstream& out)
{
	if (set.empty())
	{
		out << "None" << endl;
		return;
	}

	const int* int_p = set.first();
	while(int_p != NULL)
	{
		out << *int_p << " ";
		int_p = set.next();
	}
	out << endl;
}

