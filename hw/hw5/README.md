Homework 5/Project 2
--------------------

Run the following in terminal:
```shell
$ qmake -project
$ qmake
$ make
$ ./hw5
```
_Note_: program will ask for input database filename.  
Blank or invalid file will close the program.

You can also pass in the database file in terminal.
```shell
$ ./hw5 <database.txt>
```

After testing, to quit application,  
press the "Quit" button on the main window and provide the output database filename to save to.

To clean up
```shell
$ make clean
```