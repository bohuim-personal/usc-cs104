#include "review_item.h"

#include <iostream>
#include <QString>

#include "constants.h"

using namespace std;

/* Con/Destructor */
/**
 * @method ReviewItem
 *
 *
 */
ReviewItem::ReviewItem(Review *r)
{
	review = r;

	setupWidget();
}

ReviewItem::~ReviewItem()
{
	
}


/* Setup */
void ReviewItem::setupWidget()
{
	header = new QHBoxLayout();
		ratingLabel = new QLabel(QSTR_STRING("Rating: ") + QSTR_NUMBER(review->rating) + QSTR_STRING("/5"));
			ratingLabel->setFont( QFont("Helvetica", 11, QFont::Bold) );
		dateLabel = new QLabel(QSTR_STRING(review->date));
			dateLabel->setFont( QFont("Helvetica", 11, QFont::Bold) );
	header->addWidget(ratingLabel);
	header->addStretch();
	header->addWidget(dateLabel);

	commentLabel = new QLabel( QSTR_STRING(review->reviewText) );//.replace( QString("\n"), QString("<br/>") ) );
		commentLabel->setWordWrap(true);
		commentLabel->setFont( QFont("Helvetica", 12) );

	mainLayout = new QVBoxLayout();
	mainLayout->addLayout(header);
	mainLayout->addWidget(commentLabel);
	setLayout(mainLayout);
}