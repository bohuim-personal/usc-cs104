#ifndef CONSTANTS_H
#define CONSTANTS_H

/* Macros */
#define QSTR_DOUBLE(x)  QString::number(x, 'f', 2)
#define	QSTR_NUMBER(x)	QString::number(x)
#define QSTR_STRING(x)  QString::fromStdString(x)
#define QSTR_UTF8(x) 	QString::fromUtf8(x)

#define UTF_UP_ARROW "\u2191"
#define UTF_DOWN_ARROW "\u2193"

#endif