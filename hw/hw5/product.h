#ifndef PRODUCT_H
#define PRODUCT_H

#include "review.h"
#include <iostream>
#include <string>
#include <set>
#include <vector>
#include <algorithm>
#include <ostream>

class Product
{
    public:
        Product(const std::string category, const std::string name, double price, int qty);
        virtual ~Product();

        /* Accessors */
        std::string getCategory() const;
        std::string getName() const;
        double getPrice() const;
        int getQty() const;
        void subtractQty(int num);

        /* Given */
        //Returns the appropriate keywords that this product should be associated with
        virtual std::set<std::string> keywords() const = 0;

        //Allows for more detailed search than simple keywords
        virtual bool isMatch(std::vector<std::string>& searchTerms) const;

        //Returns a string to display the product info for hits of the search 
        virtual std::string displayString() const = 0;

        //Prints info to textfile in DB format
        virtual void dump(std::ostream& os) const;

        /* Review Addition */
        std::vector<Review*> const& getReviews();
        void addReview(Review* r);
        double getAverageRating() const;
        void dumpReviews(std::ostream& ofile);

    protected:
        std::string category_;
        std::string name_;
        double price_;
        int qty_;

        std::vector<Review*> reviews;
        int ratingSum;

};


/* Compare Functors */
struct ProductNameAsc {
    bool operator()(Product *p1, Product *p2)
    {
        return p1->getName() < p2->getName();
    }
};

struct ProductNameDesc {
    bool operator()(Product *p1, Product *p2)
    {
        return p2->getName() < p1->getName();
    }
};

struct ProductRatingAsc {
    bool operator()(Product *p1, Product *p2)
    {
        return p1->getAverageRating() < p2->getAverageRating();
    }
};

struct ProductRatingDesc {
    bool operator()(Product *p1, Product *p2)
    {
        return p2->getAverageRating() < p1->getAverageRating();
    }
};

#endif
