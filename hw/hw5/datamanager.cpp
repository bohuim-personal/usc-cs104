#include "datamanager.h"

#include "util.h"
#include <iostream>
using namespace std;

/* Helper prototypes */
void removePunct(string& word);


/* Constructor Destructor */
/**
 * @method DataManager
 * 
 * Constructor for DataManager
 * Nothing to do
 */
DataManager::DataManager()
{
	currentUser = NULL;
	currentProduct = NULL;
}

/**
 * @method ~DataManager
 * 
 * Destructor for DataManager
 * Delete all users and products
 */
DataManager::~DataManager()
{
	for (unsigned int i=0; i<products.size(); i++)
	{
		Product *p = products[i];

		vector<Review*> reviews = p->getReviews();
		for (unsigned int i = 0; i < reviews.size(); ++i)
			delete reviews[i];

		delete p;
	}

	for (unsigned int i=0; i<users.size(); i++)
		delete users[i];
}


/* DataStore Override */
/**
 * @method addUser
 * 
 * Given a pointer to a created User object,
 * adds it to the list(vector) of users,
 * and also maps the name to the pointer.
 * 
 * @param u - user pointer to add
 */
void DataManager::addUser(User* u)
{
	users.push_back(u);
	userMap[ convToLower(u->getName()) ] = u;
}

/**
 * @method addProduct
 * 
 * Given a pointer to a created product object,
 * adds it to the list(vector) of products.
 * Also gets the set of keywords associated,
 * and maps all keywords back to that product.
 * 
 * @param p - product pointer to add
 */
void DataManager::addProduct(Product* p)
{
	products.push_back(p);

	set<string> keywords = p->keywords();
	for (set<string>::iterator it = keywords.begin(); it != keywords.end(); it++)
		keyIndex[*it].insert(p);
}

/**
 * @method addReview
 * 
 * Given a new review pointer,
 * goes through list of products
 * to see a name match, 
 * and adds it to the matching product.
 * 
 * @param r - Review to add
 */
void DataManager::addReview(Review* r)
{
	for (int i=0; i < (int)products.size(); i++)
	{
		Product* p = products[i];
		if ( convToLower(r->prodName) == convToLower(p->getName()) )
		{
			p->addReview(r);
			return;
		}
	}
}

/**
 * @method dump
 * 
 * Given an output stream to a database file,
 * writes all the updated products and users
 * to the database in the pre-defined format.
 * 
 * @param ofile - ref of output stream to file
 */
void DataManager::dump(ostream& ofile)
{
	ofile << "<products>" << endl;
	for (unsigned int i=0; i<products.size(); i++)
		products[i]->dump(ofile);
	ofile << "</products>" << endl;

	ofile << "<users>" << endl;
	for (unsigned int i=0; i<users.size(); i++)
		users[i]->dump(ofile);
	ofile << "</users>" << endl;

	ofile << "<reviews>" << endl;
	for (unsigned int i=0; i<products.size(); i++)
		products[i]->dumpReviews(ofile);
	ofile << "</reviews>" << endl;
}

/**
 * @method search
 * 
 * Given search terms, and type of relation (AND/OR)
 * returns the vector of Products* that match the search.
 * 
 * @param terms - words to search with
 * @param type  - relation of terms (AND/OR)
 * @return vector of products meeting the search requirements
 */
vector<Product*> DataManager::search(vector<string>& terms, int type)
{
	vector<Product*> hits;
	if (terms.empty())
		return hits;

	//first word
	set<Product*> match = keyIndex[ terms[0] ];

	//after
	for (unsigned int i=1; i<terms.size(); i++)
	{
		//type 0: AND, type 1: OR
		if (type == 0) match = setIntersection(match, keyIndex[ terms[i] ]);
		else           match = setUnion(match, keyIndex[ terms[i] ]);
	}
	
	//http://stackoverflow.com/questions/5034211/c-copy-set-to-vector
	copy(match.begin(), match.end(), back_inserter(hits));
	return hits;
}


/* Accessors */
/**
 * @method getUsers
 * 
 * Returns a const ref to list of user pointers.
 * @note given vector cannot be changed in any way,
 *       but the Users inside are mutable
 *
 * @return immutable vector of mutable user pointers
 */
vector<User*> const& DataManager::getUsers() const
{
	return users;
}

/**
 * @method getProducts
 * 
 * Returns a const ref to list of product pointers.
 * @note given vector cannot be changed in any way,
 *       but the Products inside are mutable
 *
 * @return immutable vector of mutable product pointers
 */
vector<Product*> const& DataManager::getProducts() const
{
	return products;
}

/**
 * @method getCurrentUser
 * 
 * Returns the currently set user
 */
User* DataManager::getCurrentUser() const
{
	return currentUser;
}

/**
 * @method getCurrentProduct
 * 
 * Returns the currently set product
 */
Product* DataManager::getCurrentProduct() const
{
	return currentProduct;
}

/**
 * @method setCurrentUser
 * 
 * Sets the current user to the given one
 * @param u - pointer to new user
 */
void DataManager::setCurrentUser(User* u)
{
	currentUser = u;
}

/**
 * @method setCurrentUser
 *
 * Sets the current user to the one 
 * at the given position in user list.
 * 
 * @param pos - index of new user in user list
 */
void DataManager::setCurrentUser(int pos)
{
	currentUser = users[pos];
}

/**
 * @method setCurrentProduct
 * 
 * Sets the current product to the given one
 */
void DataManager::setCurrentProduct(Product* p)
{
	currentProduct = p;
}


/* Shopping Functionality */
void DataManager::addToCart()
{
	if (currentProduct == NULL)
	{
		cout << "No Product selected" << endl;
		return;
	}

	currentUser->addToCart(currentProduct);
}


/* Private Helpers */
/**
 * @function removePunct
 * 
 * Given a ref to a string word,
 * removes all punctuations and replaces word.
 *
 * @param word - to remove punctuations from
 */
void removePunct(string& word)
{
	string newWord = "";
	for (unsigned int i=0; i<word.length(); i++)
		if (!ispunct(word[i]))
			newWord += word[i];
	word = newWord;
}