#ifndef AMAZON_H
#define AMAZON_H

#include <QWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QDialog>
#include <QLineEdit>
#include <QCloseEvent>
#include <QFont>
#include <QApplication>
#include <QDesktopWidget>

#include <iostream>
#include <string>
#include <fstream>

/* Class Prototypes */
class DBParser;
class DataManager;
class SearchBox;
class UserSidebar;

class Amazon : public QWidget
{
	Q_OBJECT;

	private:
		//Model
		DataManager *dm;
		bool parseSuccess;
		std::string filename;

		//View
		QHBoxLayout *mainLayout;
			QVBoxLayout *mainCol;
				QLabel *titleLabel;
				SearchBox *searchBox;
			QVBoxLayout *sideCol;
				UserSidebar *sidebar;
				QPushButton *closeButton;

		//Open dialog
		QDialog *loadDialog;
		QVBoxLayout *loadDialogLayout;
			QHBoxLayout *loadInputLayout;
				QLabel *loadMessage;
				QLineEdit *ifileInput;
			QHBoxLayout *loadToolbar;
				QPushButton *cancelLoadButton;
				QPushButton *loadButton;

		//Close dialog
		QDialog *saveDialog;
		QVBoxLayout *saveDialogLayout;
			QHBoxLayout *saveInputLayout;
				QLabel *saveMessage;
				QLineEdit *ofileInput;
			QHBoxLayout *saveToolbar;
				QPushButton *dontSaveButton;
				QPushButton *cancelSaveButton;
				QPushButton *saveButton;

	public:
		Amazon(const char* filename);
		virtual ~Amazon();

		bool parseSucceeded();
		void closeEvent(QCloseEvent *event);

		void updateSearchBox();

	private:
		void parse(const char* filename);
		std::string getFilename();

		void setup();
		void setupLoadDialog();
		void setupSaveDialog();

	private slots:
		bool close();
		void cancelSave();
		void dontSave();
		void save(bool save = true);
};

#endif