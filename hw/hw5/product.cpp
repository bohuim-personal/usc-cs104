#include <sstream>
#include <iomanip>
#include "product.h"

#include "msort.h"

using namespace std;

Product::Product(const std::string category, const std::string name, double price, int qty) : 
  category_(category),
  name_(name),
  price_(price),
  qty_(qty)
{
    ratingSum = 0;
}

Product::~Product()
{

}


/* Accessors */
string Product::getCategory() const
{
    return category_;
}

string Product::getName() const
{
    return name_;
}

double Product::getPrice() const
{
    return price_;
}

void Product::subtractQty(int num)
{
    qty_ -= num;
}

int Product::getQty() const
{
    return qty_;
}


/* Other Methods */
//default implementation...can be overriden in a future assignment 
bool Product::isMatch(vector<string>& searchTerms) const
{
    return searchTerms.size();
}

/**
 * @method dump
 * 
 * Dumps the basic info about this product 
 * to the given output ostream in a DB format
 * 
 * @param os - output stream to print to
 */
void Product::dump(ostream& os) const
{
    os << category_ << endl;
    os << name_     << endl;
    os << price_    << endl;
    os << qty_      << endl;
}


/* Review Addition */
/**
 * @method getReviews
 *
 * Returns a const ref to vector of reviews
 * attached to this particular product
 */
 vector<Review*> const& Product::getReviews()
 {
    mergeSort(reviews, ReviewDateDesc());
    return reviews;
 }

/**
 * @method addReview
 *
 * Adds the given review to the 
 * list of reviews attached to this product
 *
 * @param r - new review to add
 */
 void Product::addReview(Review* r)
 {
    reviews.push_back(r);
    ratingSum += r->rating;
 }

/**
 * @method averageRating
 *
 * Returns average rating of this product
 * given by the reviews.
 */
 double Product::getAverageRating() const
 {
    if (reviews.empty())
        return -1;
    return ratingSum / (double)reviews.size();
 }

 void Product::dumpReviews(ostream& ofile)
 {
    for (int i=0; i < (int)reviews.size(); ++i)
    {
        Review* r = reviews[i];
        ofile << r->prodName   << endl;
        ofile << r->rating     << " ";
        ofile << r->date       << " ";
        ofile << r->reviewText << endl;
    }
 }