#ifndef BOOK_H
#define BOOK_H

#include "product.h"
using namespace std;

class Book : public Product
{
	public:
		Book(string title, string author, string ISBN, double price, int qty);
		~Book();

		set<string> keywords() const;
		bool isMatch(vector<string>& searchTerms) const;
		string displayString() const;
		void dump(ostream& os) const;

	private:
		string author;
		string ISBN;
};

#endif