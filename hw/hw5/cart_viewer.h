#ifndef CART_VIEWER_H
#define CART_VIEWER_H

#include "user.h"
#include "product.h"

#include <iostream>
#include <vector>

#include <QWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QHeaderView>
#include <QPushButton>
#include <QLabel>
#include <QFont>
#include <QList>
#include <QCloseEvent>

class UserSidebar;

class CartViewer : public QWidget
{
	Q_OBJECT;

	private:
		//View
		QVBoxLayout *mainLayout;

		//info row
		QHBoxLayout *infoRow;
			QLabel  *cartLabel;
			QLabel  *balanceLabel;
			QLabel  *cartBalanceLabel;
			QLabel  *cartCountLabel;

		//content
		QHBoxLayout *contentLayout;
			QTreeWidget *cartDisplay;
			QVBoxLayout *sideCol;
				QPushButton *buyCartButton;
				QPushButton *removeButton;
				QPushButton *closeButton;

		//Model
		UserSidebar *parent;
		User *user;

	public:
		CartViewer(UserSidebar *sb, User *u);
		virtual ~CartViewer();

		void update();
		void updateUserInfo();
		void updateCartDisplay();

	public slots:
		void closeCart();

	private slots:
		void buyCart();
		void remove();
};

#endif