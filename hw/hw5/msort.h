#ifndef MSORT_H
#define MSORT_H

#include <vector>

#include <iostream>
using namespace std;


/* Prototype */
template <class T, class Comparator>
void mergeSort(vector<T>& v, Comparator comp);

template <class Iter, class Comparator>
void mergeSortHelper(Iter begin, Iter end, Comparator comp);

template<class Iter, class Comparator>
void merge(Iter begin, Iter mid, Iter end, Comparator comp);


/* Implementations */

/**
 * @function mergeSort
 *
 *
 * @param v    - vector to sort
 * @param comp - compare functor
 */
template <class T, class Comparator>
void mergeSort(vector<T>& v, Comparator comp)
{
	mergeSortHelper(v.begin(), v.end(), comp);
}

/**
 * @function mergeSortHelper
 *
 *
 *
 * @param v     - vector to sort
 * @param begin - start index to sort
 * @param end   - end index to sort
 * @param comp  - compare functor
 */
template <class Iter, class Comparator>
void mergeSortHelper(Iter begin, Iter end, Comparator comp)
{
	if (end - begin > 1)
	{
		Iter mid = begin + (end - begin)/2;
		mergeSortHelper(begin, mid, comp);
		mergeSortHelper(mid, end, comp);
		merge(begin, mid, end, comp);
	}
}

template<class Iter, class Comparator>
void merge(Iter begin, Iter mid, Iter end, Comparator comp)
{
	typedef typename iterator_traits<Iter>::value_type T;

	Iter it1 = begin;
	Iter it2 = mid;

	//insert to temp until one runs out
	vector<T> temp;
	while( (it1 < mid) && (it2 < end) )
	{
		if (comp(*it2, *it1))
		{
			temp.push_back(*it2);
			++it2;
		}
		else
		{
			temp.push_back(*it1);
			++it1;
		}
	}

	//add all of remaining pile
	for(Iter it = it1; it < mid; ++it)
		temp.push_back(*it);
	for(Iter it = it2; it < end; ++it)
		temp.push_back(*it);

	Iter it = temp.begin();
	while( begin != end ) 
		*begin++ = *it++;
}

#endif