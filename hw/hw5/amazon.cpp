#include "amazon.h"

#include "constants.h"
#include "db_parser.h"
#include "datamanager.h"
#include "search_box.h"
#include "user_sidebar.h"

using namespace std;

/* Constructor & Destructor */
/**
 * @method Amazon
 * 
 * Constructor for Amazon program.
 * Takes in a c string filename.
 * If null, opens a LoadDialog to ask user for database filename.
 * Depending on whether user cancels and parse succeeds,
 * calls setup() to actually create GUI.
 * 
 * @param f - c string for filename passed by Terminal
 */
Amazon::Amazon(const char* f)
{
	saveDialog = NULL;
	parseSuccess = false;

	filename = (f) ? string(f) : getFilename();
	parse(filename.c_str());
	
	if (parseSuccess) 
		setup();	
}

/**
 * @method ~Amazon
 * 
 * Destructor for Amazon.
 * Deletes datamanager.
 * Qt objects are not touched.
 */
Amazon::~Amazon()
{
	delete dm;
}


/*----- Parse Helpers ------------------------------*/
/**
 * @method getFilename
 * 
 * Sets up and opens a InputDialog
 * to retreive a filename from user.
 * If user cancels, returns empty string
 * (gauranteed to fail at DBParser).
 *
 * @return string of database filename
 */
string Amazon::getFilename()
{
	setupLoadDialog();
	if (!loadDialog->exec())
		return "";

    //no error
    return ifileInput->text().toStdString();
}

/**
 * @method parse
 * 
 * Given a filename,
 * creates a new DatManager and local parser
 * to parse db file and put data into dm.
 * Sets success flag.
 * 
 * @param filename - c string of db filename
 */
void Amazon::parse(const char* filename)
{
	//load db
	dm = new DataManager;
    DBParser parser;

    // Instantiate the individual product parsers we want
    parser.addProductParser(new ProductBookParser);
    parser.addProductParser(new ProductClothingParser);
    parser.addProductParser(new ProductMovieParser);
    if( parser.parse(filename, *dm) )
    {
    	//error
        parseSuccess = false;
        return;
    }
   	parseSuccess = true;
}


/*----- Public Methods ------------------------------*/
/**
 * @method parseSucceeded
 * 
 * Returns whether parse was successful.
 */
bool Amazon::parseSucceeded()
{
	return parseSuccess;
}

/**
 * @method closeEvent
 * 
 * Called when user clicks "x" button on window.
 * Accept the close event and see if user 
 * actually wants to quit by calling close().
 *
 * @param event - close event
 */
void Amazon::closeEvent(QCloseEvent *event)
{
	if (close()) 
		event->accept();
	else
		event->ignore();
}

/** 
 * @method updateSearchBox
 *
 * Updates the searchBox
 */
void Amazon::updateSearchBox()
{
	searchBox->update();
}


/*----- Private Slots ------------------------------*/
/**
 * @method close
 * 
 * Called when user wants to close app.
 * If not already, create SaveDialog,
 * and check where user wants to save to.
 */
bool Amazon::close()
{
	if (saveDialog == NULL)
	{
		setupSaveDialog();
	}

	if (saveDialog->exec())
	{
		QApplication::exit();
		return true;
	}
	return false;
}

/**
 * @method cancelSave
 * 
 * Called when user presses "Cancel" button on SaveDialog.
 * Rejects the dialog, and cancel app quit.
 */
void Amazon::cancelSave()
{
	saveDialog->reject();
}

/**
 * @method dontSave
 * 
 * Called when user presse "Don't Save" button on SaveDialog.
 * Calls save() with false.
 */
void Amazon::dontSave()
{
	save(false);
}

/**
 * @method save
 * 
 * Called to save or not save to a output db file.
 * If user wants to save,
 * get desired filename and dump DataManager's data.
 * @note This method closes the app regardless of saving.
 *
 * @param save - flag to save or not
 */
void Amazon::save(bool save)
{
	if (save)
	{
		string filename = ofileInput->text().toStdString();

		if (filename.empty())
		{
			//some error
			return;
		}

		ofstream out( filename.c_str() );
		dm->dump(out);
		out.close();
	}
	saveDialog->accept();
}


/*----- Setup Methods ------------------------------*/
/**
 * @method setup
 * 
 * Sets up the main Amazon widget.
 * Then moves it to the center of screen.
 */
void Amazon::setup()
{
	setWindowTitle("Amazon");

	/* Main Column */
	mainCol = new QVBoxLayout();
		titleLabel = new QLabel("Amazon");
			titleLabel->setFont( QFont("Helvetica", 18, QFont::Bold) );
		searchBox = new SearchBox(this, dm);
	mainCol->addWidget(titleLabel, 0, Qt::AlignBottom);
	mainCol->addWidget(searchBox);

	/* Side Column */
	sideCol = new QVBoxLayout();
		sidebar = new UserSidebar(this, dm);
		closeButton = new QPushButton("Quit");
			closeButton->setFixedWidth(100);
			connect(closeButton, SIGNAL(clicked()), this, SLOT(close()));
	sideCol->addWidget(sidebar);
	sideCol->addWidget(closeButton, 0, Qt::AlignRight);

	/* Main Window Layout */
	mainLayout = new QHBoxLayout();
	mainLayout->addLayout(mainCol);
	mainLayout->addLayout(sideCol);
	setLayout(mainLayout);

	//move to center: https://forum.qt.io/topic/21035/how-to-move-the-window-to-the-center-of-the-screen/6
    adjustSize();
    move( QApplication::desktop()->screen()->rect().center() - rect().center() );
}

/**
 * @method setupSaveDialog
 * 
 * Sets up the SaveDialog to be used when exiting app.
 */
void Amazon::setupSaveDialog()
{
	saveDialog = new QDialog(this);
	saveDialogLayout = new QVBoxLayout();
		//inputs
		saveInputLayout = new QHBoxLayout();
			saveMessage = new QLabel("Save to: ");
			ofileInput = new QLineEdit( QSTR_STRING(filename) );
		saveInputLayout->addWidget(saveMessage);
		saveInputLayout->addWidget(ofileInput);
		//save toolbar
		saveToolbar = new QHBoxLayout();
			dontSaveButton = new QPushButton("Don't Save");
				connect(dontSaveButton, SIGNAL(clicked()), this, SLOT( dontSave() ));
			cancelSaveButton = new QPushButton("Cancel");
				connect(cancelSaveButton, SIGNAL(clicked()), this, SLOT( cancelSave() ));
			saveButton = new QPushButton("Save");
				saveButton->setDefault(true);
				connect(saveButton, SIGNAL(clicked()), this, SLOT( save() ));
		saveToolbar->addWidget(dontSaveButton);
		saveToolbar->addStretch();
		saveToolbar->addWidget(cancelSaveButton);
		saveToolbar->addWidget(saveButton);
	saveDialogLayout->addLayout(saveInputLayout);
	saveDialogLayout->addLayout(saveToolbar);
	saveDialog->setLayout(saveDialogLayout);
	saveDialog->setFixedWidth(400);
}

/**
 * @method setupLaodDialog
 * 
 * Sets up LoadDialog to be used when loading app.
 */
void Amazon::setupLoadDialog()
{
	loadDialog = new QDialog(this);
	loadDialogLayout = new QVBoxLayout();
		//inputs
		loadInputLayout = new QHBoxLayout();
			loadMessage = new QLabel("Load from: ");
			ifileInput = new QLineEdit("database.txt");
		loadInputLayout->addWidget(loadMessage);
		loadInputLayout->addWidget(ifileInput);
		//load toolbar
		loadToolbar = new QHBoxLayout();
			cancelLoadButton = new QPushButton("Cancel");
				connect(cancelLoadButton, SIGNAL(clicked()), loadDialog, SLOT(reject()));
			loadButton = new QPushButton("Load");
				loadButton->setDefault(true);
				connect(loadButton, SIGNAL(clicked()), loadDialog, SLOT(accept()));
		loadToolbar->addStretch();
		loadToolbar->addWidget(cancelLoadButton);
		loadToolbar->addWidget(loadButton);
	loadDialogLayout->addLayout(loadInputLayout);
	loadDialogLayout->addLayout(loadToolbar);
	loadDialog->setLayout(loadDialogLayout);
	loadDialog->setFixedWidth(400);
}