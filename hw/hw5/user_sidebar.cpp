#include "user_sidebar.h"

#include "amazon.h"
#include "cart_viewer.h"
#include "constants.h"
#include "user.h"
#include "datamanager.h"

using namespace std;

/* ----- Con & Destructor ------------------------ */
/**
 * @method UserSidebar
 * 
 * Constructor for UserSidebar.
 * Takes in a DataManager to communicate
 * the data accross the diffent QWidgets.
 * DataManager acts much like a singleton class.
 *
 * Calls setup to setup visuals and connect signals.
 *
 * @param p - parent Amazon class
 * @param ndm - DataManager that holds users and products
 */
UserSidebar::UserSidebar(Amazon *p, DataManager *ndm)
{
	parent = p;
	dm = ndm;
	setup();
}

/**
 * @method ~UserSidebar
 * 
 * Destructor for UserSidebar.
 * Traverses through cartMap to delete all CartViewers.
 * All other are Qt objects, no need to explicitly clean.
 */
UserSidebar::~UserSidebar()
{
	for (cartMapType::iterator it = cartMap.begin(); it != cartMap.end(); ++it)
		delete it->second;
}


/* ----- Setup Methods ------------------------ */
/**
 * @method setup
 * 
 * Setups of this UserSidebar column widget.
 * Creates instances of all necessary widgets
 * and inputs appropriate initial data into them.
 */
void UserSidebar::setup()
{
	/* Dropbox User Select */
	userLabel = new QLabel(QSTR_STRING("Hello,"));
	userSelect = new QComboBox();
		userSelect->setFixedWidth(150);

		vector<User*> users = dm->getUsers();
		for (int i=0; i < (int)users.size(); ++i)
			userSelect->addItem( QSTR_STRING(users[i]->getName()) );

		connect(userSelect, SIGNAL(currentIndexChanged(int)), this, SLOT(setCurrentUser(int)));

	/* Cart buttons */
	addToCartButton = new QPushButton("Add to Cart");
		connect(addToCartButton, SIGNAL(clicked()), this, SLOT(addToCart()));
	viewCartButton = new QPushButton("View Cart");
		connect(viewCartButton, SIGNAL(clicked()), this, SLOT(viewCart()));
	closeAllButton = new QPushButton("Close All Carts");
		connect(closeAllButton, SIGNAL(clicked()), this, SLOT(closeAll()));

	/* Current User Info */
	userBalanceLabel = new QLabel();
	cartBalanceLabel = new QLabel();
	cartCountLabel = new QLabel();

	/* Main Layout Column */
	mainLayout = new QVBoxLayout();
	mainLayout->addWidget(userLabel);
	mainLayout->addWidget(userSelect);
	mainLayout->addWidget(addToCartButton);
	mainLayout->addWidget(viewCartButton);
	mainLayout->addWidget(closeAllButton);
	mainLayout->addWidget(userBalanceLabel);
	mainLayout->addWidget(cartBalanceLabel);
	mainLayout->addWidget(cartCountLabel);
	mainLayout->addStretch();
	setLayout(mainLayout);

	setCurrentUser(0);
}


/* ----- Public Methods ------------------------ */
/**
 * @method update
 * 
 * Updates labels, all carts, 
 * and tell parent to update searchBox.
 */
void UserSidebar::update()
{
	//user info
	updateUserInfo();

	//carts
	for (cartMapType::iterator it = cartMap.begin(); it != cartMap.end(); ++it)
		(it->second)->update();

	//update search box
	parent->updateSearchBox();
}

/**
 * @method updateUserInfo
 *
 * Updates the labels below buttons.
 */
void UserSidebar::updateUserInfo()
{
	//user info
	User *u = dm->getCurrentUser();
	userBalanceLabel->setText( QSTR_STRING("Balance: $") + QSTR_NUMBER(u->getBalance()) );
	cartBalanceLabel->setText( QSTR_STRING("Cart Total: $") + QSTR_NUMBER(u->getCartBalance()) );
	cartCountLabel->setText( QSTR_STRING("Items: ") + QSTR_NUMBER(u->getCartSize()) );
}


/* ----- Private Slots ------------------------ */
/**
 * @method setCurrentUser
 * 
 * Called when user switches currently shopping user.
 * Sets the current user to the one at given index
 * and updates the current user's info
 *
 * @param index - of currently selected user in user list
 */
void UserSidebar::setCurrentUser(int index)
{
	dm->setCurrentUser(index);
	updateUserInfo();
}

/**
 * @method addToCart
 * 
 * Called when users presses "Add to Cart" button.
 * Tells datamanager to add the current product to current user
 * and updates the current user's info
 */
void UserSidebar::addToCart()
{
	dm->addToCart();
	updateUserInfo();

	CartViewer *cv = cartViewerForUser( dm->getCurrentUser() );
	cv->updateCartDisplay();
	cv->updateUserInfo();
}

/**
 * @method viewCart
 * 
 * Called when user presses "View Cart" button.
 * Shows the CartViewer window of the currently selected user.
 */
void UserSidebar::viewCart()
{
	CartViewer *cv = cartViewerForUser( dm->getCurrentUser() );
	cv->show();
	cv->raise();
	cv->activateWindow();
}

/**
 * @method closeAll
 *
 * Called when user presses "Close All Carts" button.
 * Close all carts open
 */
void UserSidebar::closeAll()
{
	for (cartMapType::iterator it = cartMap.begin(); it != cartMap.end(); ++it)
		(it->second)->closeCart();
}


/* ----- Private Helpers ------------------------ */
/**
 * @method cartViewerForUser
 * 
 * Returns the corresponding CartViewer for the user.
 * If not already in the map, create it.
 * @note manual check is necessary because CartViewer
 * 		 does not have a default constructor
 */
CartViewer* UserSidebar::cartViewerForUser(User *u)
{
	if (cartMap.find(u) == cartMap.end())
		cartMap[u] = new CartViewer(this, u);

	return cartMap[u];
}