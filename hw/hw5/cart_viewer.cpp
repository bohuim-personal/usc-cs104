#include "cart_viewer.h"

#include "constants.h"
#include "user_sidebar.h"

#include <iostream>
#include <string>
#include <vector>

using namespace std;

/* ----- Con & Destructor ------------------------ */
/**
 * @method CartViewer
 * 
 * Constructor for CartViewer.
 * Takes in a UserSidebar (which acts like a parent)
 * and a User to represent his/her cart.
 * Sets up all necessary components
 * and connects view to controller.
 */
CartViewer::CartViewer(UserSidebar *sb, User *u)
{
	parent = sb;
	user = u;

	/* Info Row */
	infoRow = new QHBoxLayout();
		cartLabel = new QLabel(QSTR_STRING(user->getName()));
			QFont cartLabelFont("Helvetica", 18, QFont::Bold);
			cartLabel->setFont(cartLabelFont);
		balanceLabel = new QLabel();
		cartBalanceLabel = new QLabel();
		cartCountLabel = new QLabel();
		updateUserInfo();
	infoRow->addWidget(cartLabel);
	infoRow->addWidget(balanceLabel,     0, Qt::AlignBottom);
	infoRow->addWidget(cartBalanceLabel, 0, Qt::AlignBottom);
	infoRow->addWidget(cartCountLabel,   0, Qt::AlignBottom);

	/* Cart Display */
	cartDisplay = new QTreeWidget();
		cartDisplay->setColumnCount(5);
		cartDisplay->setSortingEnabled(false);
		cartDisplay->setSelectionMode(QAbstractItemView::MultiSelection);
		cartDisplay->setHeaderLabels(QStringList() << "Name" << "Price" << "Category" << "Rating" << "Quantity");
		cartDisplay->setFixedWidth(780);
		cartDisplay->setMinimumHeight(300);
		cartDisplay->header()->resizeSection(0, 360);

	/* Side Column */
	sideCol = new QVBoxLayout();
		buyCartButton = new QPushButton("Buy Cart");
			connect(buyCartButton, SIGNAL(clicked()), this, SLOT(buyCart()));
		removeButton = new QPushButton("Remove Item(s)");
			connect(removeButton, SIGNAL(clicked()), this, SLOT(remove()));
		closeButton = new QPushButton("Close");
			connect(closeButton, SIGNAL(clicked()), this, SLOT(closeCart()));
	sideCol->addWidget(buyCartButton);
	sideCol->addWidget(removeButton);
	sideCol->addStretch();
	sideCol->addWidget(closeButton);

	/* Content Layout */
	contentLayout = new QHBoxLayout();
	contentLayout->addWidget(cartDisplay);
	contentLayout->addLayout(sideCol);

	/* Main Layout */
	mainLayout = new QVBoxLayout();
	mainLayout->addLayout(infoRow);
	mainLayout->addLayout(contentLayout);
	setWindowTitle(QSTR_STRING("Cart: ") + QSTR_STRING(user->getName()));
	setLayout(mainLayout);
}

/**
 * @method ~CartViewer
 *
 * Destructor for CartViewer.
 * All Qt objects, nothing to explicitly clean.
 */
CartViewer::~CartViewer()
{
	
}


/* ----- Public Methods ------------------------ */
/**
 * @method updateUserInfo
 *
 * Updates both user info and cart
 */
void CartViewer::update()
{
	updateUserInfo();
	updateCartDisplay();
}

/**
 * @method updateUserInfo
 * 
 * Updates info regarding the User and Cart.
 */
void CartViewer::updateUserInfo()
{
	balanceLabel     -> setText( QSTR_STRING("Balance: $") + QSTR_DOUBLE(user->getBalance()) ); 
	cartBalanceLabel -> setText( QSTR_STRING("Cart Total: $") + QSTR_DOUBLE(user->getCartBalance()) );
	cartCountLabel   -> setText( QSTR_STRING("Items: ") + QSTR_NUMBER(user->getCartSize()) );
}

/**
 * @method updateCartDisplay
 * 
 * Updates cartDisplay according to contents of User's cart.
 * First clear anything in the display.
 * Retreive cart from user, 
 * and create a TreeWidgetItem for each Product.
 */
void CartViewer::updateCartDisplay()
{
	cartDisplay->clear();

	vector<Product*> cart = user->getCart();
	for (int i=0; i < (int)cart.size(); ++i)
	{
		Product* p = cart[i];

		QTreeWidgetItem *item = new QTreeWidgetItem(cartDisplay);
		item->setText(0, QSTR_STRING(p->getName()));
		item->setText(1, QSTR_DOUBLE(p->getPrice()));
		item->setText(2, QSTR_STRING(p->getCategory()));

		double rating = p->getAverageRating();
		item->setText(3, (rating < 0) ? QSTR_STRING("N/A") : QSTR_DOUBLE(rating));

		item->setText(4, QSTR_NUMBER(p->getQty()));
	}
}


/* ----- Private Slots ------------------------ */
/**
 * @method buyCart
 * 
 * Called when user clicks the "Buy Cart" button.
 * Tell user to buy the cart and update display.
 * Update parent's user info in real time, too.
 */
void CartViewer::buyCart()
{
	user->buyCart();

	update();
	parent->update(); //calls update on this cart too
}

/**
 * @method remove
 * 
 * Called when users clicks the "Remove Item(s)" button.
 * Retreive a list of selected items to remove.
 * Go through the user's actually cart in reverse order
 * and remove the items.
 */
void CartViewer::remove()
{
	QList<QTreeWidgetItem*> selected = cartDisplay->selectedItems();
	for (int i = selected.size()-1; i >= 0; --i)
	{
		int index = cartDisplay->indexOfTopLevelItem(selected[i]);
		user->removeFromCart(index);
	}
	updateCartDisplay();
}

/**
 * @method closeCart
 * 
 * Called when user presses the "Close" button.
 * Closes this CartViewer widget.
 */
void CartViewer::closeCart()
{
	this->close();
}