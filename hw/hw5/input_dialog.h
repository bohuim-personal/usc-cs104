#ifndef INPUT_DIALOG_H 
#define INPUT_DIALOG_H

#include <QWidget>
#include <QDialog>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>

#include <string>

enum Status {
	noStatus,
	statusRejected,
	statusCanceled,
	statusAccepted
};

class InputDialog : public QDialog
{
	Q_OBJECT;

	private:
		//view
		QVBoxLayout *mainLayout;
			QHBoxLayout *inputRow;
				QLabel *message;
				QLineEdit *input;
			QHBoxLayout *toolbar;
				QPushButton *rejectButton;
				QPushButton *cancelButton;
				QPushButton *acceptButton;

		//model
		bool rejectEnabled;
		Status stat;

	public:
		InputDialog(bool addRejectButton);
		virtual ~InputDialog();

		void setMessage(const std::string& msg);
		void setReject(const std::string& msg);
		void setCancel(const std::string& msg);
		void setAccept(const std::string& msg);

		std::string text() const;

	private:
		void setup();

	public slots:
		int exec();
		void reject();
		void accept();
		void cancel();
};

#endif