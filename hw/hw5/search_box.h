#ifndef SEARCH_BOX_H
#define SEARCH_BOX_H

#include <QWidget>
#include <QHBoxLayout>
#include <QLabel>
#include <QComboBox>
#include <QLineEdit>
#include <QPushButton>
#include <QTreeWidget>

#include <iostream>
#include <string>
#include <sstream>
#include <string>
#include <vector>

enum SortState {
	nameAsc = 0,
	nameDesc,
	ratingAsc,
	ratingDesc
};

/* Class Prototypes */
class Amazon;
class ReviewBox;
class Product;
class DataManager;

class SearchBox : public QWidget
{
	Q_OBJECT;

	private:
		//Model
		Amazon *parent;
		DataManager *dm;
		std::vector<Product*> hits;
		SortState sortState;

		//View
		QVBoxLayout *mainLayout;
			//Search Bar
			QHBoxLayout *searchBar;
				QComboBox 	*typeSelect;
				QLineEdit	*searchField;
				QPushButton *searchButton;
			//Search Result
			QTreeWidget *searchResult;
			//Toolbar
			QHBoxLayout *toolbar;
				QPushButton *nameSortButton;
				QPushButton *ratingSortButton;
				QPushButton *clearButton;
			//Review
			ReviewBox *reviewBox;

	public:
		SearchBox(Amazon *p, DataManager* ndm);
		virtual ~SearchBox();

		void update();

	private:
		//setup
		void setupWidget();
		void setupLayout();

		void sort(SortState sortBy);
		void updateSearchResults();

	private slots:
		void search();
		void sortByName();
		void sortByRating();
		void clearSearch();
		void setCurrentItem();
};

#endif