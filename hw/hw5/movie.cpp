#include "movie.h"
#include "util.h"

#include <iostream>
#include <sstream>
using namespace std;

/**
 * @method Movie
 * 
 * Constructor for movie.
 * Takes in necessary params for both movie and Product
 * Passes the Product vars to Product's constructor
 * and sets the others
 *
 * @param title  - of movie
 * @param size 	 - of movie
 * @param brand  - of movie
 * @param price  - of product
 * @param qty    - initial quantity
 */
Movie::Movie(string title, string genre, string rating, double price, int qty) : Product("movie", title, price, qty)
{
	this->genre = genre;
	this->rating = rating;
}

Movie::~Movie()
{

}

/* Override */
/**
 * @method keywords
 * 
 * Returns the set of keywords associated with this movie.
 * Runs movie's name and title through parseStringToWords
 * and gets the union of the two returned sets.
 * Adds ISBN to the set (doesn't need to be parsed).
 *
 * @return set of keyword that relate to this movie
 */
set<string> Movie::keywords() const
{
	set<string> genreWords = parseStringToWords(genre);
	set<string> nameWords = parseStringToWords(name_);
	set<string> keywords = setUnion(genreWords, nameWords);

	return keywords;
}

/**
 * @method isMatch
 * 
 * Given a list of search terms,
 * checks whether they match with this movie
 * 
 * @param searchTerms - to check with this movie
 * @return whether given search terms match this movie
 */
bool Movie::isMatch(vector<string>& searchTerms) const
{
	set<string> keys = keywords();
	for (unsigned int i=0; i<searchTerms.size(); i++)
		if ( keys.find(searchTerms[i]) != keys.end() )
			return true;
	return false;
}

/**
 * @method displayString
 * 
 * Returns the description of this movie
 * in a specific format for displaying.
 * 
 * @param formatted summary string of this movie
 */
string Movie::displayString() const
{
	ostringstream converter;

	//main desc
	string display = "";
	display += name_ + "\n";
	display += "Genre: " + genre + " " + "Rating: " + rating + "\n";

	//price
	converter << price_;
	display += converter.str() + " ";

	//qty left
	converter.str("");
	converter.clear();
	converter << qty_;
	display += converter.str() + " left" + "\n";

	return display;
}

/**
 * @method dump
 * 
 * Given an output stream, 
 * writes the info for this movie
 * in a specified database format.
 *
 * @param os - outputstream to write db info to
 */
void Movie::dump(ostream& os) const
{
	Product::dump(os);
	os << genre  << endl;
	os << rating << endl;
}