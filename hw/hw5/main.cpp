#include <QApplication>

#include "amazon.h"

#include <iostream>
using namespace std;

int main(int argc, char* argv[])
{
    char *filename = NULL;
    if (argc > 1)
        filename = argv[1];

    QApplication app(argc, argv);

	Amazon amazon(filename);

    if (!amazon.parseSucceeded())
        return 1;

    amazon.show();
	return app.exec();
}