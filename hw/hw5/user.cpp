#include "user.h"
using namespace std;

User::User() : name_("unknown"), age_(0), balance_(0.0), type_(1)
{
	cartBalance = 0;
}
User::User(std::string name, int age, double balance, int type) : 
			name_(name), age_(age), balance_(balance), type_(type)
{
	cartBalance = 0;
}

User::~User()
{

}


/* Accessors */
int User::getAge() const
{
	return age_;
}

std::string User::getName() const
{
	return name_;
}

double User::getBalance() const
{
	return balance_;
}

void User::deductAmount(double amt)
{
	balance_ -= amt;
}

void User::dump(std::ostream& os)
{
	os << name_ << " " << age_ << " " << balance_ << " " << type_ << endl;
}


/* Cart Additions */
int User::getCartSize() const
{
	return cart.size();
}

double User::getCartBalance() const
{
	return cartBalance;
}

vector<Product*> const& User::getCart() const
{
	return cart;
}

void User::buyCart()
{
	int index = 0;
	while (index < (int)cart.size())
	{
		Product* product = cart[index];
		if ( (product->getQty() > 0) && (getBalance() >= product->getPrice()) )
		{
			product->subtractQty(1);
			deductAmount(product->getPrice());

			cart.erase(cart.begin() + index);
			index--;
		}
		index++;
	}
}

void User::addToCart(Product *p)
{
	cart.push_back(p);
}

void User::removeFromCart(int i)
{
	cart.erase( cart.begin() + i );
}