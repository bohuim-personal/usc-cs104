#include "search_box.h"

#include <QStringList>
#include <QTreeWidgetItem>
#include <QHeaderView>
#include <QString>

#include "review_box.h"
#include "util.h"
#include "msort.h"
#include "constants.h"
#include "product.h"
#include "datamanager.h"

using namespace std;


/* Constructor Destructor */
/**
 * @method SearchBox
 * 
 * Constructor for SearchBox.
 * Saves a poniter to DataManger passed by Amazon main program,
 * and sets inital sort state to unsorted.
 * 
 * Calls setup methods.
 *
 * @param p - parent Amazon class
 * @param ndm - new datamanager to save
 */
SearchBox::SearchBox(Amazon *p, DataManager* ndm)
{
	parent = p;
	dm = ndm;
	sortState = nameAsc;
	
	setupWidget();
	setupLayout();
}

/**
 * @method ~SearchBox
 * 
 * Destructor for SearchBox.
 * All Qt objects, nothing to explicitly clean.
 */
SearchBox::~SearchBox()
{

}


/* ----- Setup Methods ------------------------ */
/**
 * @method setupWidget
 * 
 * Sets up the layout of SearchBox widget.
 * @note Does not touch any sizing policies.
 */
void SearchBox::setupWidget()
{
	/* Search Bar */
	searchBar = new QHBoxLayout();
		// AND|OR drop menu
		typeSelect = new QComboBox();
			typeSelect->addItem(QSTR_STRING("OR"));
			typeSelect->addItem(QSTR_STRING("AND"));
		// main search field
		searchField = new QLineEdit();
			searchField->setPlaceholderText(QSTR_STRING("Search"));
			connect(searchField, SIGNAL(returnPressed()), this, SLOT(search()));
		// search button
		searchButton = new QPushButton("Go");
			connect(searchButton, SIGNAL(clicked()), this, SLOT(search()));
	searchBar->addWidget(typeSelect);
	searchBar->addWidget(searchField);
	searchBar->addWidget(searchButton);

	/* Search Result */
	searchResult = new QTreeWidget();
		searchResult->setColumnCount(5);
		searchResult->setSortingEnabled(false);
		//searchResult->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
		searchResult->setHeaderLabels(QStringList() << "Name" << "Price" << "Category" << "Rating" << "Quantity");
		searchResult->header()->resizeSection(0, 360);
		searchResult->header()->resizeSection(4, 80);
		connect(searchResult, SIGNAL( currentItemChanged(QTreeWidgetItem*, QTreeWidgetItem*) ),
				this, SLOT( setCurrentItem() ));

	/* Toolbar */
	toolbar = new QHBoxLayout();
		//sort by name
		nameSortButton = new QPushButton(QSTR_UTF8("Name " UTF_DOWN_ARROW));
			nameSortButton->setCheckable(true);
			nameSortButton->setChecked(true);
			connect(nameSortButton, SIGNAL(clicked()), this, SLOT(sortByName()));
		//sort by rating
		ratingSortButton = new QPushButton(QSTR_UTF8("Rating " UTF_DOWN_ARROW));
			ratingSortButton->setCheckable(true);
			connect(ratingSortButton, SIGNAL(clicked()), this, SLOT(sortByRating()));
		//clear search
		clearButton = new QPushButton("Clear");
			connect(clearButton, SIGNAL(clicked()), this, SLOT(clearSearch()));
	toolbar->addWidget(nameSortButton);
	toolbar->addWidget(ratingSortButton);
	toolbar->addStretch();
	toolbar->addWidget(clearButton);

	/* Review Container */
	reviewBox = new ReviewBox(this);

	/* Main Layout */
	mainLayout = new QVBoxLayout();
	mainLayout->addLayout(searchBar);
	mainLayout->addWidget(searchResult);
	mainLayout->addLayout(toolbar);
	mainLayout->addWidget(reviewBox);
	setLayout(mainLayout);
}

/**
 * @method setupLayout
 * 
 * Sets up all necessary sizing policies.
 */
void SearchBox::setupLayout()
{
	typeSelect->setFixedWidth(60);
	searchButton->setFixedWidth(50);

	searchResult->setMinimumWidth(765);
	searchResult->setMinimumHeight(250);

	nameSortButton->setFixedWidth(100);
	ratingSortButton->setFixedWidth(100);
	clearButton->setFixedWidth(80);
}


/* ----- Public Mehtods ----------------------- */
/** 
 * @method update
 *
 * Sort by the current sortState and update display.
 */
void SearchBox::update()
{
	sort( sortState );
	updateSearchResults();
}


/* ----- Private Slots ------------------------ */
/**
 * @method search
 * 
 * Called when user searches (return or Search button).
 * Clears any previous searches.
 * Takes search terms & type
 * newly sets hits and redisplays results.
 */
void SearchBox::search()
{
	int type = (typeSelect->currentText() == "AND") ? 0 : 1;
	QString searchText = searchField->text();

	clearSearch();
	searchField->setText( searchText );

	string term;
	vector<string> terms;
	istringstream ss( searchText.toLower().toStdString() );
	while(ss >> term)
		terms.push_back(term);

	hits = dm->search(terms, type);
	sort(sortState);
}

/**
 * @method sortByName
 * 
 * Called when user presses "Name ^" button.
 * If not sorted or in desc,
 * 		sort by asc name (A->Z)
 * Otherwise (sorted by asc),
 * 		sort by desc rating (Z->A)
 *
 * Set/modify any other fields,
 * and rest Rating Sort Button
 */
void SearchBox::sortByName()
{
	sortState = (sortState == nameAsc) ? nameDesc : nameAsc;
	sort(sortState);
}

/**
 * @method sortByRating
 * 
 * Called when user presses "Rating ^" button.
 * If not sorted or in desceding,
 * 		sort by asc rating (N/A -> 5 star)
 * Otherwise (sorted by asc),
 * 		sort by desc rating (5 -> N/A)
 * 
 * Set/modify any other fields
 * and reset Name Sort Button
 */
void SearchBox::sortByRating()
{
	sortState = (sortState == ratingDesc) ? ratingAsc : ratingDesc;
	sort(sortState);
}

/**
 * @method clear
 * 
 * Called when user presses "Clear" button.
 * Clears search field, results, and hits.
 */
void SearchBox::clearSearch()
{
	hits.clear();
	searchField->clear();
	searchResult->clear();
	dm->setCurrentProduct(NULL);
	reviewBox->setProduct(NULL);
}

/**
 * @method setCurrentItem
 * 
 * Called when user clicks a new item to select it.
 * Retreive the currently clicked item's index
 * and set the corresponding product as the 
 * current product in datamanager.
 */
void SearchBox::setCurrentItem()
{
	QTreeWidgetItem *currentItem = searchResult->currentItem();
	if (currentItem == NULL)
		return;

	int index = searchResult->indexOfTopLevelItem(currentItem);
	Product* p = hits[index];

	dm->setCurrentProduct(p);
	reviewBox->setProduct(p);
}


/* ----- Private Helpers ------------------------ */
/**
 * @method sort
 *
 * Given a sort description, sorts search results.
 * Modifies and resets button aesthetics accordingly.
 * @note exits if results size is 1 or less
 *
 * @param sortBy - criterion to sort by
 */
void SearchBox::sort(SortState sortBy)
{
	//buttons
	QString arrow = (sortBy == nameAsc || sortBy == ratingDesc) ? 
				  	QSTR_UTF8(UTF_DOWN_ARROW) : QSTR_UTF8(UTF_UP_ARROW);
	if (sortBy == nameAsc || sortBy == nameDesc)
	{
		nameSortButton->setText( QSTR_STRING("Name ") + arrow );
		nameSortButton->setChecked(true);

		ratingSortButton->setChecked(false);
		ratingSortButton->setText( QSTR_UTF8("Rating " UTF_DOWN_ARROW) );
	}
	else
	{
		ratingSortButton->setText( QSTR_STRING("Rating ") + arrow );
		ratingSortButton->setChecked(true);

		nameSortButton->setChecked(false);
		nameSortButton->setText( QSTR_UTF8("Name " UTF_DOWN_ARROW) );
	}

	//sorting
	if (hits.empty())
		return;

	if (sortBy == nameAsc)
		mergeSort(hits, ProductNameAsc());
	else if (sortBy == nameDesc)
		mergeSort(hits, ProductNameDesc());
	else if (sortBy == ratingAsc)
		mergeSort(hits, ProductRatingAsc());
	else if (sortBy == ratingDesc)
		mergeSort(hits, ProductRatingDesc());
	updateSearchResults();
}

/**
 * @method updateSearchResults
 * 
 * Shows the search results on the TreeWidget.
 * First clear any previous results,
 * traverse through hits and convert
 * Product* into a TreeWidgetItem repsentation
 */
void SearchBox::updateSearchResults()
{
	//save lastSelected Product's name
	QTreeWidgetItem *lastSelected = searchResult->currentItem();
	string name;
	if (lastSelected != NULL)
		name = lastSelected->text(0).toStdString();

	searchResult->clear();
	for (int i=0; i < (int)hits.size(); ++i)
	{
		Product* p = hits[i];

		QTreeWidgetItem *item = new QTreeWidgetItem(searchResult);
		item->setText(0, QSTR_STRING(p->getName()));
		item->setText(1, QSTR_DOUBLE(p->getPrice()));
		item->setText(2, QSTR_STRING(p->getCategory()));

		double rating = p->getAverageRating();
		item->setText(3, (rating < 0) ? QSTR_STRING("N/A") : QSTR_DOUBLE(rating));

		item->setText(4, QSTR_NUMBER(p->getQty()));

		if (lastSelected != NULL && (name == p->getName()))
			searchResult->setCurrentItem(item);
	}
}