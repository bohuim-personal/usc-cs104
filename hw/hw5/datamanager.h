#ifndef DATAMANGER_H
#define DATAMANGER_H

#include "datastore.h"
#include "review.h"
#include <ostream>
#include <string>
#include <utility>
#include <vector>
#include <map>
#include <set>

class DataManager : public DataStore
{
	public:
		DataManager();
		~DataManager();

		/* DataStore override */
		void addUser(User* u);
		void addProduct(Product* p);
		void addReview(Review *r);
		void dump(std::ostream& ofile);
		std::vector<Product*> search(std::vector<std::string>& terms, int type);

		/* Accessors */
		std::vector<User*> const& getUsers() const;
		std::vector<Product*> const& getProducts() const;
		User* getCurrentUser() const;
		Product *getCurrentProduct() const;
		void setCurrentUser(User* u);
		void setCurrentUser(int pos);
		void setCurrentProduct(Product *);

		/* Cart Methods */
		void addToCart();

    private:
    	//Current
    	User* currentUser;
    	Product* currentProduct;

    	//list of all items
    	std::vector<User*> users;
    	std::vector<Review*> reviews;
    	std::vector<Product*> products;

    	//maps
    	std::map< std::string, User* > userMap;

    	//search index
    	std::map< std::string, std::set<Product*> > keyIndex;
};

#endif