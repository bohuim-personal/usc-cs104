#ifndef USER_H
#define USER_H

#include <iostream>
#include <string>
#include <vector>

#include "product.h"

/**
 * Implements User functionality and information storage
 *  You should not need to derive anything from User at this time
 */
class User
{
    public:
        User();
        User(std::string name, int age, double balance, int type);
        virtual ~User();

        int getAge() const;
        double getBalance() const;
        std::string getName() const;
        void deductAmount(double amt);
        virtual void dump(std::ostream& os);

        /* Cart Additions */
        int getCartSize() const;
        double getCartBalance() const;
        std::vector<Product*> const& getCart() const;
        void buyCart();
        void addToCart(Product* p);
        void removeFromCart(int i);

    private:
        std::string name_;
        int age_;
        double balance_;
        int type_;

        double cartBalance;
        std::vector<Product*> cart;
};
#endif
