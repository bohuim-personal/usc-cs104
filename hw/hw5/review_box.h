#ifndef REVIEW_BOX_H
#define REVIEW_BOX_H

#include <QWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QScrollArea>
#include <QLabel>
#include <QDate>
#include <QTextEdit>
#include <QPushButton>
#include <QComboBox>
#include <QDialog>
#include <QFont>
#include <QLayoutItem>
#include <QCalendarWidget>

#include <iostream>
#include <string>
#include <vector>

class Review;
class Product;
class DataManager;
class SearchBox;

class ReviewBox : public QWidget
{
	Q_OBJECT;

	private:
		//View
		QVBoxLayout *mainLayout;
			QLabel *reviewLabel;
			//Review Form
			QVBoxLayout *reviewForm;
				QHBoxLayout *topToolbar;
					QLabel    *ratingLabel;
					QComboBox *ratingSelect;

					QPushButton *dateButton;
					QLabel *dateLabel;
				QHBoxLayout *postLayout;
					QTextEdit *commentInput;
					QPushButton *postButton;
			//Calendar
			QDialog *calWidget;
				QVBoxLayout *calLayout;
					QCalendarWidget *cal;
					QHBoxLayout *calToolbar;
						QPushButton *cancelButton;
						QPushButton *okButton;
			//Reviews
			QScrollArea *reviewScrollArea;
				QWidget *reviewDisplayWrap;
				QVBoxLayout *reviewDisplay;

		//Model
		Product *product;
		SearchBox *parent;

	public:
		ReviewBox(SearchBox *sb);
		virtual ~ReviewBox();

		void setProduct(Product *p);
		void clearReviewDisplay();
		void updateReviewDisplay();

	private:
		void setupWidget();
		void setupLayout();

	private slots:
		void updateDate();
		void updateDate(const QDate& date);
		void postReview();
		void showCalendar();
};

#endif