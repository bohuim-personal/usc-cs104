#ifndef USER_SIDEBAR_H
#define USER_SIDEBAR_H

#include <QWidget>
#include <QVBoxLayout>
#include <QLabel>
#include <QComboBox>
#include <QPushButton>
#include <QApplication>

#include <map>
#include <string>
#include <iostream>

/* Class Prototypes */
class Amazon;
class CartViewer;
class User;
class DataManager;

class UserSidebar : public QWidget
{
	Q_OBJECT;

	private:
		//GUI
		QVBoxLayout *mainLayout;
		//user select
			QLabel    *userLabel;
			QComboBox *userSelect;
		//user info
			QLabel *userBalanceLabel;
			QLabel *cartBalanceLabel;
			QLabel *cartCountLabel;
		//cart button
			QPushButton *addToCartButton;
			QPushButton *viewCartButton;
			QPushButton *closeAllButton;

		//Model
		Amazon *parent;
		DataManager *dm;

		typedef std::map< User*, CartViewer* > cartMapType;
		cartMapType cartMap;

	public:
		UserSidebar(Amazon *p, DataManager *ndm);
		virtual ~UserSidebar();

		void update();
		void updateUserInfo();

	private slots:
		void setCurrentUser(int index);
		void addToCart();
		void viewCart();
		void closeAll();

	private:
		void setup();
		CartViewer* cartViewerForUser(User* u);
};

#endif