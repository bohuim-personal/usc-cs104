#ifndef REVIEW_ITEM_H
#define REVIEW_ITEM_H

#include <QWidget>
#include <QLabel>
#include <QVBoxLayout>
#include <QHBoxLayout>

#include "review.h"

class ReviewItem : public QWidget
{
	Q_OBJECT;

	private:
		//Model
		Review *review;

		//View
		QVBoxLayout *mainLayout;
			QHBoxLayout *header;
				QLabel *ratingLabel;
				QLabel *dateLabel;
			QLabel *commentLabel;

	public:
		ReviewItem(Review *r);
		virtual ~ReviewItem();

	private:
		void setupWidget();
		void setupLayout();
};

#endif