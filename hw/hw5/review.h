#ifndef REVIEW_H
#define REVIEW_H

#include <string>
#include <cstdlib>
#include <iostream>

/**
 * Models a review for the product identified by prodName
 */
struct Review
{
    Review() :  prodName(), rating(), date(), reviewText() { }
    Review(std::string prod, int rate, std::string d, std::string review_text) 
    : prodName(prod), rating(rate), date(d), reviewText(review_text) { }
  
    std::string prodName;
    int rating;
    std::string date; //YYYY-MM-DD
    std::string reviewText;
};

struct ReviewDateAsc
{
	bool operator()(Review *r1, Review *r2)
	{
		using namespace std;

		string date1 = r1->date;
		string date2 = r2->date;

		int y1 = atoi( date1.substr(0, 4).c_str() );
		int y2 = atoi( date2.substr(0, 4).c_str() );
		if (y1 < y2) return true;
		if (y1 > y2) return false;

		int m1 = atoi( date1.substr(5, 7).c_str() );
		int m2 = atoi( date2.substr(5, 7).c_str() );
		if (m1 < m2) return true;
		if (m1 > m2) return false;

		int d1 = atoi( date1.substr(8, 10).c_str() );
		int d2 = atoi( date2.substr(8, 10).c_str() );
		if (d1 < d2) return true;
		if (d2 > d1) return false;

		return false;
	}
};

struct ReviewDateDesc
{
	bool operator()(Review *r1, Review *r2)
	{
		ReviewDateAsc asc;
		return !asc(r1, r2);
	}
};

#endif
