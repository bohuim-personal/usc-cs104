#include "input_dialog.h"

#include <iostream>
#include <string>

#include "constants.h"

using namespace std;

InputDialog::InputDialog(bool addRejectButton)
//: QDialog(p)
{
	stat = noStatus;
	rejectEnabled = addRejectButton;

	//setParent(p);
	setup();
}

InputDialog::~InputDialog()
{

}


/* Setup */
void InputDialog::setup()
{
	inputRow = new QHBoxLayout();
		message = new QLabel("Input: ");
		input = new QLineEdit();
	inputRow->addWidget(message);
	inputRow->addWidget(input);

	toolbar = new QHBoxLayout();
		cancelButton = new QPushButton("Cancel");
			connect(cancelButton, SIGNAL(clicked()), this, SLOT(cancel()));
		acceptButton = new QPushButton("Ok");
			acceptButton->setDefault(true);
			connect(acceptButton, SIGNAL(clicked()), this, SLOT(accept()));
	toolbar->addStretch();
	toolbar->addWidget(cancelButton);
	toolbar->addWidget(acceptButton);

	setReject("Quit");

	mainLayout = new QVBoxLayout();
	mainLayout->addLayout(inputRow);
	mainLayout->addLayout(toolbar);
	setLayout(mainLayout);
	setFixedWidth(400);
}

void InputDialog::setMessage(const string& msg)
{
	message->setText( QSTR_STRING(msg) );
}

void InputDialog::setReject(const string& msg)
{
	if (rejectButton == NULL)
	{
		rejectButton = new QPushButton();
			connect(rejectButton, SIGNAL(clicked()), this, SLOT(reject()));
		toolbar->insertWidget(0, rejectButton);
	}
	rejectButton->setText( QSTR_STRING(msg) );
}

void InputDialog::setCancel(const string& msg)
{
	cancelButton->setText( QSTR_STRING(msg) );
}

void InputDialog::setAccept(const string& msg)
{
	acceptButton->setText( QSTR_STRING(msg) );
}

string InputDialog::text() const
{
	return input->text().toStdString();
}


/* Slots */
int InputDialog::exec()
{
	QDialog::exec();
	return stat;
}

void InputDialog::reject()
{
	stat = statusRejected;
	QDialog::accept();
}

void InputDialog::cancel()
{
	stat = statusCanceled;
	QDialog::reject();
}

void InputDialog::accept()
{
	stat = statusAccepted;
	QDialog::accept();
}