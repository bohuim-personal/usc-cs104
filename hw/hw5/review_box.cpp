#include "review_box.h"

#include "review.h"
#include "product.h"
#include "datamanager.h"
#include "constants.h"
#include "review_item.h"
#include "search_box.h"

using namespace std;

/* ----- Con & Destructor ------------------------ */
/**
 * @method ReviewBox
 * 
 * Constructor for ReviewBox.
 * Calls setup methods
 */
ReviewBox::ReviewBox(SearchBox *sb)
{
	parent = sb;

	setupWidget();
	setupLayout();
}

/**
 * @method ~ReviewBox
 * 
 * Destructor for ReviewBox.
 * All Qt objects, nothing to explicitly clean.
 */
ReviewBox::~ReviewBox()
{
	
}


/* ----- Setup Methods ------------------------ */
/**
 * @method setupWidget
 * 
 * Setups up the frame of this Widget.
 * Adds necessary layouts and widgets
 * and connects singals to slots.
 */
void ReviewBox::setupWidget()
{
	reviewLabel = new QLabel("Reviews");
		reviewLabel->setFont( QFont("Helvetica", 12, QFont::Bold) );

	/* Top Toolbar */
	topToolbar = new QHBoxLayout();
		ratingLabel = new QLabel("Rating: ");
			QFont f("Helvetica", 12, QFont::Bold);
		ratingSelect = new QComboBox();
			for (int i=5; i>=1; --i)
				ratingSelect->addItem(QSTR_NUMBER(i));

		dateLabel = new QLabel();
			updateDate(QDate::currentDate());
		dateButton = new QPushButton("Date");
			connect(dateButton, SIGNAL(clicked()), this, SLOT(showCalendar()));
	topToolbar->addWidget(ratingLabel);
	topToolbar->addWidget(ratingSelect);
	topToolbar->addStretch();
	topToolbar->addWidget(dateLabel);
	topToolbar->addWidget(dateButton);

	/* Calendar */
	calWidget = new QDialog(this);
		calLayout = new QVBoxLayout();
			cal = new QCalendarWidget();
				cal->setMinimumDate( QDate(2000, 1, 1) );
				connect(cal, SIGNAL(selectionChanged()), this, SLOT( updateDate() ));

			calToolbar = new QHBoxLayout();
				cancelButton = new QPushButton("Cancel");
					connect(cancelButton, SIGNAL(clicked()), calWidget, SLOT(reject()));
				okButton = new QPushButton("Ok");
					connect(okButton, SIGNAL(clicked()), calWidget, SLOT(accept()));
			calToolbar->addStretch();
			calToolbar->addWidget(cancelButton);
			calToolbar->addWidget(okButton);
		calLayout->addWidget(cal);
		calLayout->addLayout(calToolbar);
	calWidget->setLayout(calLayout);

	/* Bottom Toolbar */
	postLayout = new QHBoxLayout();
		commentInput = new QTextEdit();
		postButton = new QPushButton("Post Review");
			connect(postButton, SIGNAL(clicked()), this, SLOT(postReview()));
	postLayout->addWidget(commentInput);
	postLayout->addWidget(postButton);

	/* Review Form */
	reviewForm = new QVBoxLayout();
	reviewForm->addLayout(topToolbar);
	reviewForm->addLayout(postLayout);

	/* Review Display */
	reviewScrollArea = new QScrollArea();
		reviewDisplayWrap = new QWidget();
		reviewDisplay = new QVBoxLayout(reviewDisplayWrap);
			reviewDisplay->addStretch();
	reviewScrollArea->setWidget(reviewDisplayWrap);
	reviewScrollArea->setWidgetResizable(true);

	mainLayout = new QVBoxLayout();
	mainLayout->addWidget(reviewLabel);
	mainLayout->addLayout(reviewForm);
	mainLayout->addWidget(reviewScrollArea);
	mainLayout->addStretch();
	setLayout(mainLayout);
}

/**
 * @method setupLayout
 * 
 * Sets up any necessary size policies.
 */
void ReviewBox::setupLayout()
{
	dateButton->setFixedWidth(60);

	commentInput->setFixedHeight(60);

	postButton->setFixedWidth(100);
	postButton->setFixedHeight(60);

	reviewScrollArea->setMinimumHeight(200);
	reviewScrollArea->setFrameShape(QFrame::NoFrame);
}


/* ----- Public Methods ------------------------ */
/**
 * @method setProduct
 * 
 * Sets current product as the given one,
 * and updates the ReviewDisplay to 
 * show the reviews attached to this product.
 * @note NULL is allowed, and it will reset/clear display.
 *
 * @param p - pointer to Product to set
 */
void ReviewBox::setProduct(Product *p)
{
	if (p == NULL)
	{
		reviewLabel->setText("Reviews");
		clearReviewDisplay();
		return;
	}

	product = p;
	reviewLabel->setText( QSTR_STRING("Reviews: ") + QSTR_STRING(p->getName()) );
	updateReviewDisplay();
}

/**
 * @method clearReviewDisplay
 * 
 * Clears the ReviewDisplay by removing all widgets,
 * and freeing the memory associated.
 * @note guaranteed to be all Widgets in ReviewDisplay
 */
void ReviewBox::clearReviewDisplay()
{
	int count = reviewDisplay->count();
	for (int i = count-2; i >= 0; --i)
	{
		QWidget *w = reviewDisplay->itemAt(i)->widget();
		reviewDisplay->removeWidget(w);
		delete w;
	}
}

/**
 * @method updateReviewDisplay
 * 
 * Updates the ReviewDisplay by
 * first clearing anything currently in there,
 * and re-populates the view with new items.
 */
void ReviewBox::updateReviewDisplay()
{
	clearReviewDisplay();

	vector<Review*> reviews = product->getReviews();
	for (int i=0; i < (int)reviews.size(); ++i)
	{
		ReviewItem *item = new ReviewItem(reviews[i]);
		reviewDisplay->insertWidget(i, item);
	}
}

/**
 * @method updateDate
 * 
 * Sets the dateLabel to show the given QDate
 * in the pre-defined YYYY-MM-DD format.
 */
void ReviewBox::updateDate()
{	
	QDate date = cal->selectedDate();
	dateLabel->setText( date.toString("yyyy-MM-dd") );
}

/**
 * @method updateDate
 * 
 * Sets the dateLabel to show the given QDate
 * in the pre-defined YYYY-MM-DD format.
 *
 * @param date - to set dateLabel to
 */
void ReviewBox::updateDate(const QDate& date)
{
	dateLabel->setText( date.toString("yyyy-MM-dd") );
}


/* ----- Private Slots ------------------------ */
/**
 * @method postReview
 * 
 * Called when user presses "Post Review".
 * If proudct and comment is not empty
 * (by default: rating is 5, date is today),
 * add a new Review to the product,
 * and update ReviewDisplay.
 */
void ReviewBox::postReview()
{
	if (product == NULL)
	{
		//some kind of error
		return;
	}

	string comment = commentInput->toPlainText().toStdString();
	if (comment.empty())
	{
		//show some error message here
		return;
	}

	product->addReview(new Review(product->getName(), 
								  5 - ratingSelect->currentIndex(), 
								  dateLabel->text().toStdString(),
								  comment));
	updateReviewDisplay();

	//reset stuff
	updateDate(QDate::currentDate());
	ratingSelect->setCurrentIndex(0);
	commentInput->clear();
	parent->update();
}

/**
 * @method showCalendar
 * 
 * Called when user presses "Date" button.
 * Show the CalWidget by calling exec() on it.
 * If the users selects a date and presses "ok",
 * update the dateLabel accordingly.
 * Otherwise, user pressed "cancel".
 */
void ReviewBox::showCalendar()
{
	okButton->setDefault(true);
	cal->setSelectedDate(QDate::currentDate());

	if (calWidget->exec())
		updateDate(cal->selectedDate());
}