Problem 1
---------
a) README.md unmodified
b) README.md modified, fun_problem.txt untracked
c) both are staged
d) both are modified
e) README.md staged, fun_problem.txt unmodified
   fun_problem.txt is empty because it was checked out to before any changes were made
f) README.md is both staged and unstaged modified
   Everything added beforehand was staged, but echoing "Fancy git move" makes only that line modified


Problem 6
---------
empty() = Θ(1)
push()  = Θ(1)
top()   = Θ(1)
pop()   = Θ(1)

- empty() simply checks whether the underlaying linked-list's size is 0.
  Because this is independent of number of elements in the list, it's Θ(1).
- push() inserts the new value at position 0 of the linked-list.
  No traversal of the elements is required (only pointer modification); therefore, Θ(1).
- top() does not modify the list in any way, and simply returns value at position 0.
  This operation is yet again independent of all other elements in the list: Θ(1).
- pop(), similar to top(), is only concerned with removed the top/first element.
  Whatever the size of the stack, or internal linked-list, operation is constant: Θ(1).