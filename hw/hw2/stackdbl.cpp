#include "stackdbl.h"

/**
 * @method StackDbl
 * 
 * Constructor for Stack,
 * set store to -1 for when top() is called with no elements.
 */
StackDbl::StackDbl()
{
	store = -1;
}

/**
 * @method ~StackDbl
 * 
 * Destructor for Stack,
 * nothing needs to be cleaned up
 */
StackDbl::~StackDbl()
{
	
}

/**
 * @method empty
 * 
 * Returns whether the stack is empty.
 * Simply checks whether list is empty.
 *
 * @return true if no elements; otherwise, no.
 */
bool StackDbl::empty() const
{
	return list_.empty();
}

/**
 * @method push
 * 
 * Pushes the given value into the stack.
 * Inserts val at position 0 of list.
 * 
 * @param val to push
 */
void StackDbl::push(const double& val)
{
	list_.insert(0, val);
}

/**
 * @method top
 * 
 * Returns the top element of the stack.
 * Simply gives element at pos 0 of list.
 * If stack i
 *
 * @return value at top of stack
 */
double const& StackDbl::top() const
{
	if (list_.empty()) return store;
	return list_.get(0);
}

/**
 * @method pop
 * 
 * Pops, or removes, the top item of the stack.
 * Removes element at position 0 of list.
 */
void StackDbl::pop()
{
	if (!list_.empty())
		list_.remove(0);
}