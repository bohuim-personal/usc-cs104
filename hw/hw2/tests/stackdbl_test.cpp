#include "../stackdbl.h"

#include <iostream>
#include <cstdlib>
using namespace std;

/**
 * @function throwError
 * 
 * Convenience function for printing to cerr.
 * Then exits problem with code 1.
 * @param const char string error to print
 */
void throwError(const char* err)
{
	cerr << "ERROR: " << err << endl;
}


/**
 * @function main
 * 
 * Tests the StackDbl class for implementing an stack correctly.
 * Calls various methods implemented and checks that the postcondition
 * is indeed what the expected behavior of a stack.
 * If not, an error is thrown to cerr and program is ended.
 */
int main(int argc, char* argv[])
{
	StackDbl stack;

	cout << "check stack.empty()" << endl;
	if (!stack.empty()) throwError("stack not initialized empty");
	cout << endl;

	stack.push(1.0);
	cout << "stack.push(1.0)" << endl;
	if (stack.empty()) 		throwError("empty() is true. Stack not incrementing size correctly.");
	if (stack.top() != 1.0) throwError("top() not 1.0. Stack not pushing correctly.");
	cout << endl;

	stack.push(2.0);
	cout << "stack.push(2.0)" << endl;
	if (stack.top() != 2.0) throwError("top() not 2.0. Stack not pushing correctly.");
	cout << endl;

	stack.pop();
	cout << "stack.pop()" << endl;
	if (stack.top() != 1.0) throwError("top() not 1.0 after popping. Stack not popping correctly.");
	cout << endl;

	stack.pop();
	cout << "stack.pop()" << endl;
	if (!stack.empty()) throwError("empty() not true. Stack not decrementing size correctly");
	cout << endl;
}