#include "../llistdbl.h"

#include <iostream>
#include <string>
#include <cstdlib>
using namespace std;

/**
 * @function printList
 * 
 * Given a linked list, prints the list to cout.
 * @note This function is extremely inefficient in that LListDbl
 * 		 does not open its head/tail pointers nor its getNodeAt().
 *		 Therefore, each call to get(n) is of runtime O(n).
 *
 * @param const ref to LListDbl
 */
void printList(const LListDbl& list)
{
	cout << "list: ";
	for (int i=0; i<list.size(); i++)
		cout << list.get(i) << " ";
	cout << endl;
}

/**
 * @function throwError
 * 
 * Convenience function for printing to cerr.
 * Then exits problem with code 1.
 * @param const char string error to print
 */
void throwError(const char* err)
{
	cerr << "ERROR: " << err << endl;
	exit(1);
}

/**
 * @function main
 * 
 * Tests the LListDbl class for implementing an linked list correctly.
 * Calls various methods implemented and checks that the postcondition
 * is indeed what the expected behavior of an linked list is.
 * If not, an error is thrown to cerr and program is ended.
 */
int main(int argc, char* argv[])
{
	LListDbl list;

	cout << "check list.empty()" << endl;
	if (!list.empty()) throwError("list not initialized empty");
	cout << endl;

	list.insert(1, 1.0);
	cout << "list.insert(1, 1.0)" << endl;
	printList(list);
	if (!list.empty()) throwError("adding to index larger than size");
	cout << endl;

	list.insert(0, 1.0);
	cout << "list.inert(0, 1.0)" << endl;
	printList(list);
	if (list.get(0) != 1.0) throwError("get(0) not 1.0");
	if (list.size() != 1)   throwError("size() not 1. Size not incremented correctly.");
	if (list.empty())  		throwError("empty() not implemented correctly.");
	cout << endl;

	list.insert(1, 2.0);
	cout << "list.inert(1, 2.0)" << endl;
	printList(list);
	if (list.get(1) != 2.0) throwError("get(1) not 2.0. Insert at pos size not working correctly.");
	if (list.size() != 2)	throwError("size() not 2. Size not incrementing correctly.");
	cout << endl;

	list.insert(0, 3.0);
	cout << "list.insert(0, 3.0)" << endl;
	printList(list);
	if (list.get(0) != 3.0) throwError("get(0) not 3.0. Insert at pos 0 not working correctly.");
	cout << endl;

	list.remove(0);
	cout << "list.remove(0)" << endl;
	printList(list);
	if (list.get(0) != 1.0) throwError("get(0) not 2.0 after removing pos 0. Remove not working correctly");
	if (list.size() != 2)   throwError("size() not 2. Size not decrementing correctly.");
	cout << endl;

	list.remove(list.size()-1);
	cout << "list.remove(list.size()-1)" << endl;
	printList(list);
	if (list.get(0) != 1.0) throwError("get(0) not 2.0 after removing pos size-1. Remove not working correctly");
	cout << endl;

	list.remove(0);
	cout << "list.remove(0)" << endl;
	printList(list);
	if (!list.empty()) throwError("empty() not true after removing all items.");
	cout << endl;
}

