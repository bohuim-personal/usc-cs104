#include "stackdbl.h"
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <stdlib.h>

using namespace std;

/* Function Prototypes */
void convert(const char* iFile, const char* oFile);

double calculate(string& expr, bool& success);
bool isOperation(const string& input);
bool popTwo(StackDbl& stack, double& val1, double& val2);
bool apply(const string& operation, const double& val1, const double& val2, double& result);

/**
 * @function main
 * 
 * Calls convert() using the given input and output files.
 *
 * @note if there aren't enough arguments, throws an error.
 * 
 * @param argc - number of arguments including this file
 * @param argv - arguments, first being this filename
 */
int main(int argc, char* argv[])
{
	if (argc < 3)
	{
		cerr << "Provide both an input and output file" << endl;
		return 1;
	}

	convert(argv[1], argv[2]);
}

/**
 * @function convert
 * 
 * Given an in and out file, opens read and write streams.
 * Gets every line from the input file,
 * and passes it onto calculate().
 * Writes the returned calculation to output file,
 * unless success flag is set to false.
 *
 * @param iFile - input filename
 * @param oFile - output filename
 */
void convert(const char* iFile, const char* oFile)
{
	ifstream in(iFile);
	ofstream out(oFile);

	string line;
	while(getline(in, line))
	{
		//calculate line as expression
		bool success;
		double result = calculate(line, success);
		
		//write val or invalid accordingly
		if (success) out << result << endl;
		else 		 out << "invalid" << endl;
	}
}

/**
 * @function calculate
 * 
 * Given a string, interprets it as a postfix expression,
 * and returns the calculated output, unless its invalid.
 * 
 * @param expr - string to calculate as postfix expression
 * @param success - flag to set success or failure
 */
double calculate(string& expr, bool& success)
{
	StackDbl stack;
	success = true;
	
	//in string stream
	istringstream strstream(expr);
	string token;
	while(getline(strstream, token, ' '))
	{
		//check if token is an operation
		if (isOperation(token))
		{
			//get top two values from stack and push back
			double val1, val2, result;
			success = popTwo(stack, val1, val2) && apply(token, val1, val2, result);

			if (success) stack.push(result);
			else return false;
		}
		else
		{
			//interpret token as double and push to stack
			double val = strtod(token.c_str(), NULL);
			stack.push( val );
		}
	}
	
	//if stack is empty upon finishing, invalid
	if (stack.empty())
	{
		success = false;
		return 0;
	}
	return stack.top();
}

/**
 * @function isOperation
 * 
 * Returns whether the given input is an operation.
 * Searches for: "+-*\/" and checks if result is not no position.
 *
 * @param input - to check if operation
 * @return true if either "+-*\/"
 */
bool isOperation(const string& input)
{
	return input == "+" || input == "-" || input == "*" || input == "/";
}

/**
 * @function popTwo
 * 
 * Given a ref to a stack and two doubles
 * Attempts to pop two values from that stack and set them to vals.
 * Returns false if no values to pop
 *
 * @param stack - to pop from
 * @param val1  - to set second popped value
 * @param val2  - to set first popped value
 * @return false if attempt to pop from empty stack
 */
bool popTwo(StackDbl& stack, double& val1, double& val2)
{
	for (int i=0; i<2; i++)
	{
		if(stack.empty())
			return false;

		double popped = stack.top();
		stack.pop();
		if (i == 0) val2 = popped;
		else 		val1 = popped;
	}
	return true;
}

/**
 * @function apply
 * 
 * Applies the given operation to the two values in order,
 * so that val1 is always before the operation.
 *
 * @param operation - string form of operation to perform
 * @param val1 		- 1st to apply operation to
 * @param val2		- 2nd val to apply operation to
 * @return false if not valid operator or divide by 0
 */
bool apply(const string& operation, const double& val1, const double& val2, double& result)
{
	if (operation == "+")
	{
		result = val1 + val2;
		return true;
	}
	if (operation == "-")
	{
		result = val1 - val2;
		return true;
	}
	if (operation == "*")
	{
		result = val1 * val2;
		return true;
	}
	if (operation == "/")
	{
		if (val2 == 0)
			return false;

		result = val1 / val2;
		return true;
	}
	return false;
}
