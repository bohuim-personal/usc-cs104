Homework 2
-----------

### Problem 1, 6
- Answers to problems 1 and 6 are provided in hw2.txt

### Problem 3, 4, 5
- The code for these problems are provided in their respective `<filename>.h` and `<filename>.cpp`
- Testers are also provided in `tests/<filename>Test.cpp`

To run provided testers:
- In the `hw2` directory, run `make`, which compiles the .o and testers to the `bin/` directory
- Run each test using 
	- `./bin/llistdbl_test`
	- `./bin/alistint_test`
	- `./bin/stackdbl_test`
- Once done, clean up with `make clean`

To run custom testers:
- In the `hw2` directory, run `make`, which creates the .o files to the `bin/` directory
- Compile your tester using `g++ -g -Wall <tester>.cpp <dependencies>.o -o bin/<tester_exe>`
- Run your tester using `./bin/<tester_exe>`
- Once done, clean up with `make clean`

- NOTE: make sure the tester executable names do not overlap with the provided tester executables


### Problem 7
- If the `bin/` directory was not removed after running `make`,  
simply run `./bin/postfix <inputfile> <outputfile>`

Note: invalid inputs include
- Operator without two inputs before
- Divide by 0


LATE: Bug fix where postfix would not take a negative number and any lines that end with a space character