#include "llistdbl.h"
#include <cstdlib>

LListDbl::LListDbl()
{
    head_ = NULL;
    tail_ = NULL;
    size_ = 0;
}

LListDbl::~LListDbl()
{
    clear();
}

bool LListDbl::empty() const
{
    return size_ == 0;
}

int LListDbl::size() const
{
    return size_;
}

/**
 * @method insert
 *
 * Inserts the value at the given location.
 * Method accounts for all edge cases.
 * @note nothing is modified if loc is not within [0, size]
 *
 * @param loc - to add new value at
 * @param val - new value to insert
 */
void LListDbl::insert(int loc, const double& val)
{
    //check that loc is within bounds
    if (!(0 <= loc && loc <= size_))
        return;

    //assume loc == size
    Item* newNode = new Item;
    Item* oldNode = NULL;
    Item* preNode = tail_;
    if (loc < size_)
    {
        //otherwise get old node at loc & prev node
        oldNode = getNodeAt(loc);
        preNode = oldNode->prev;
    }

    //modify surrounding nodes accor.
    if (loc == 0) head_ = newNode;
    else  preNode->next = newNode;

    if (loc == size_) tail_ = newNode;
    else      oldNode->prev = newNode;

    //setup newly created node
    newNode->val = val;
    newNode->prev = preNode;
    newNode->next = oldNode;

    size_++;
}

/**
 * @method remove
 *
 * Removes the node at the given location.
 * Method accounts for all edge cases
 * @note nothing is modified if loc is not within [0, size).
 */
void LListDbl::remove(int loc)
{
    //check loc is within bounds
    if (!(0 <= loc && loc < size_))
        return;

    //get target and surrounding nodes
    Item* node = getNodeAt(loc);
    Item* prev = node->prev;
    Item* next = node->next;

    //if node is first
    if (prev == NULL) head_ = next;
    else         prev->next = next;

    //if node is last
    if (next == NULL) tail_ = prev;
    else         next->prev = prev;

    //cleanup & decrement
    delete node;
    size_--;
}   

void LListDbl::set(int loc, const double& val)
{
    Item *temp = getNodeAt(loc);
    temp->val = val;
}

double& LListDbl::get(int loc)
{
    Item *temp = getNodeAt(loc);
    return temp->val;
}

double const & LListDbl::get(int loc) const
{
    Item *temp = getNodeAt(loc);
    return temp->val;
}

void LListDbl::clear()
{
    while(head_ != NULL)
    {
        Item *temp = head_->next;
        delete head_;
        head_ = temp;
    }
    tail_ = NULL;
    size_ = 0;
}


LListDbl::Item* LListDbl::getNodeAt(int loc) const
{
    Item *temp = head_;
    if(loc >= 0 && loc < size_)
    {
        while(temp != NULL && loc > 0)
        {
            temp = temp->next;
            loc--;
        }
        return temp;
    }
    else 
    {
        //throw std::invalid_argument("bad location");
        return NULL;
    }
}
