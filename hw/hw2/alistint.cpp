#include "alistint.h"

/**
 * @method AListInt
 * 
 * Default constructor for ArrayList,
 * capacity is 10 if not specified.
 */
AListInt::AListInt()
{
	_array = new int[10];
	_capacity = 10;
	_size = 0;
}

/**
 * @method AListInt
 * 
 * Constructor for ArrayList with the given capacity.
 * A new array of length cap is declared; size is set to 0.
 *
 * @param cap - initial length of internal array 
 */
AListInt::AListInt(int cap)
{
	_array = new int[cap];
	_capacity = cap;
	_size = 0;
}

/**
 * @method ~AListInt
 * 
 * Destructor for ArrayList.
 * Cleanups up memory of current internal array.
 */
AListInt::~AListInt()
{
	delete[] _array;
}

/**
 * @method size
 * 
 * Returns numbers of elements, not length of array.
 * @return _size - number of elements
 */
int AListInt::size() const
{
	return _size;
}

/**
 * @method empty
 * 
 * Returns whether list is empty.
 * @return true if _size is zero; otherwise, false.
 */
bool AListInt::empty() const
{
	return _size == 0;
}

/**
 * @method insert
 * 
 * Inserts the value into the given position.
 * Checks if current internal array is full,
 * and calls {@code resize()} to double length.
 * Shift everything right of pos one cell to the right.
 * Set cell at pos to val.
 *
 * @note nothing is modified if pos is not within [0, size]
 *
 * @param pos - to insert val
 * @param val - value to insert
 */
void AListInt::insert(int pos, const int& val)
{
	//check pos is within bounds
	if (!(0 <= pos && pos <= _size))
		return;

	//if full, double array & copy elements
	if (_size == _capacity)
		resize();

	for (int i=_size; i>pos; i--)
		_array[i] = _array[i-1];
	_array[pos] = val;

	_size++;
}

/**
 * @method remove
 * 
 * Removes the value at the given position.
 * Move everything right of pos one cell to the left,
 * effectively overriding and delete val at pos.
 * 
 * @param pos - to of value to remove
 */
void AListInt::remove(int pos)
{
	//check pos is within bounds
	if (!(0 <= pos && pos < _size))
		return;

	//move elements from [pos, size) one cell left
	for (int i=pos; i<_size; i++)
		_array[i] = _array[i+1];
	_array[_size] = 0;

	_size--;
}

/**
 * @method set
 * 
 * Modifies the value at pos to the given val.
 * @note nothing is modified if pos is not within [0, size)
 *
 * @param pos - to insert new value at
 * @param val - to newly insert
 */
void AListInt::set(int pos, const int& val)
{
	if (!(0 <= pos && pos < _size))
		return;

	_array[pos] = val;
}

/**
 * @method get
 * 
 * Returns the value at the given pos.
 * @note returned value CAN be modified 
 *
 * @param pos - to get value from
 * @return _array[pos]
 */
int& AListInt::get(int pos)
{
	return _array[pos];
}

/**
 * @method get
 * 
 * Returns the value at the given pos.
 * @note returned value CANNOT be modified 
 *
 * @param pos - to get value from
 * @return _array[pos]
 */
int const& AListInt::get (int position) const
{
	return _array[position];
}

/**
 * @method resize
 * 
 * Doubles the capacity, or length, of the internal array.
 * Creates a new array of twice the old capacity,
 * copies over all elements in the list to the new array,
 * then cleans up the old array
 */
void AListInt::resize()
{
	_capacity *= 2;
	int* new_Array = new int[_capacity];
	for (int i=0; i<_size; i++)
		new_Array[i] = _array[i];

	delete[] _array;
	_array = new_Array;
}