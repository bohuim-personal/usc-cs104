#ifndef INVENTORY_H
#define INVENTORY_H

#include <vector>
#include <map>
#include <string>

class Inventory : private std::map<std::string, int> {
public:
	Inventory();

	void addItem(std::string, int);
	void removeItem(std::string);
	int getNumItems(std::string);

	void printAllItems();
};

#endif