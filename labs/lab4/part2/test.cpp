#include "arraylist.h"
#include "gtest/gtest.h"

class ArrayListTest : public testing::Test {
protected:
	// You can remove any or all of the following functions if its body is empty.

	ArrayListTest() {
		// You can do set-up work for each test here.

		for(int i=1; i <= 10; i++)
			list.add(i);
	}

	virtual ~ArrayListTest() {
		// You can do clean-up work that doesn't throw exceptions here.		
	}

	// If the constructor and destructor are not enough for setting up
	// and cleaning up each test, you can define the following methods:
	virtual void SetUp() {
		// Code here will be called immediately after the constructor (right
		// before each test).
		
		//add test
		list.add(42);
	}

	virtual void TearDown() {
		// Code here will be called immediately after each test (right
		// before the destructor).
	}

	// Objects declared here can be used by all tests in the test case.
	ArrayList list;
};

TEST_F(ArrayListTest, GetNominal) {
	// Exepct 0th element to be 1, 1st elemnt to be 2, etc.
	for (int i = 1 ; i <= 10; i++) {
		EXPECT_EQ(list.get(i), i);
	}
}

TEST_F(ArrayListTest, AddNominal) {
	EXPECT_EQ(list.size(), 11);
	EXPECT_EQ(list.get(11), 42);
}

TEST_F(ArrayListTest, RemoveNominal) {
	
}

TEST_F(ArrayListTest, SetOffNominal) {
	
}
